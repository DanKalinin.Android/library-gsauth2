package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2Token extends LJLObject implements Serializable {
    public static String SUBJECT_ACCOUNT;
    public static String SUBJECT_CONTROLLER;

    public String subject;
    public String id;
    public String access;
    public String refresh;
}
