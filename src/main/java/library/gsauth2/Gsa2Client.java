package library.gsauth2;

import android.os.Handler;
import android.os.Looper;
import java.time.Instant;
import library.java.lang.LJLObject;

public class Gsa2Client extends LJLObject {
    private long object;

    public static native Gsa2Client client(String base, Gsa2Database database);

    @Override
    protected native void finalize() throws Throwable;

    // IdentityCheck

    public interface IdentityCheckCallback {
        void callback(boolean available, Gsa2Exception exception);
    }

    public void identityCheck(Gsa2Identity identity, IdentityCheckCallback callback) {
        _identityCheck(identity, (available, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(available, exception);
            });
        });
    }

    private native void _identityCheck(Gsa2Identity identity, IdentityCheckCallback callback);

    // CodeSend

    public interface CodeSendCallback {
        void callback(Instant expiration, Gsa2Exception exception);
    }

    public void codeSend(Gsa2Identity identity, CodeSendCallback callback) {
        _codeSend(identity, (expiration, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(expiration, exception);
            });
        });
    }

    private native void _codeSend(Gsa2Identity identity, CodeSendCallback callback);

    // AccountCreate

    public interface AccountCreateCallback {
        void callback(Gsa2Account account, Instant expiration, Gsa2Exception exception);
    }

    public void accountCreate(Gsa2Account account, Gsa2Identity[] identities, AccountCreateCallback callback) {
        _accountCreate(account, identities, (_account, expiration, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(_account, expiration, exception);
            });
        });
    }

    private native void _accountCreate(Gsa2Account account, Gsa2Identity[] identities, AccountCreateCallback callback);

    // AccountGet

    public interface AccountGetCallback {
        void callback(Gsa2Account account, Gsa2Exception exception);
    }

    public void accountGet(String access, AccountGetCallback callback) {
        _accountGet(access, (account, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(account, exception);
            });
        });
    }

    private native void _accountGet(String access, AccountGetCallback callback);

    // PasswordUpdate

    public interface PasswordUpdateCallback {
        void callback(Gsa2Exception exception);
    }

    public void passwordUpdate(String password, String access, PasswordUpdateCallback callback) {
        _passwordUpdate(password, access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _passwordUpdate(String password, String access, PasswordUpdateCallback callback);

    // DomainCreate

    public interface DomainCreateCallback {
        void callback(Gsa2DomainFull domainFull, Gsa2Exception exception);
    }

    public void domainCreate(Gsa2Domain domain, Gsa2ControllerFull[] controllersFull, String access, DomainCreateCallback callback) {
        _domainCreate(domain, controllersFull, access, (domainFull, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(domainFull, exception);
            });
        });
    }

    private native void _domainCreate(Gsa2Domain domain, Gsa2ControllerFull[] controllersFull, String access, DomainCreateCallback callback);

    // DomainRename

    public interface DomainRenameCallback {
        void callback(Gsa2Exception exception);
    }

    public void domainRename(String domainId, String name, String access, DomainRenameCallback callback) {
        _domainRename(domainId, name, access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _domainRename(String domainId, String name, String access, DomainRenameCallback callback);

    // DomainDelete

    public interface DomainDeleteCallback {
        void callback(Gsa2Exception exception);
    }

    public void domainDelete(String domainId, String access, DomainDeleteCallback callback) {
        _domainDelete(domainId, access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _domainDelete(String domainId, String access, DomainDeleteCallback callback);

    // DomainExit

    public interface DomainExitCallback {
        void callback(Gsa2Exception exception);
    }

    public void domainExit(String domainId, String access, DomainExitCallback callback) {
        _domainExit(domainId, access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _domainExit(String domainId, String access, DomainExitCallback callback);

    // GuestDelete

    public interface GuestDeleteCallback {
        void callback(Gsa2DomainFull domainFull, Gsa2Exception exception);
    }

    public void guestDelete(String guestId, String domainId, String access, GuestDeleteCallback callback) {
        _guestDelete(guestId, domainId, access, (domainFull, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(domainFull, exception);
            });
        });
    }

    private native void _guestDelete(String guestId, String domainId, String access, GuestDeleteCallback callback);

    // InvitationCreate

    public interface InvitationCreateCallback {
        void callback(Gsa2Invitation invitation, Gsa2Exception exception);
    }

    public void invitationCreate(Gsa2Invitation invitation, String domainId, String access, InvitationCreateCallback callback) {
        _invitationCreate(invitation, domainId, access, (_invitation, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(_invitation, exception);
            });
        });
    }

    private native void _invitationCreate(Gsa2Invitation invitation, String domainId, String access, InvitationCreateCallback callback);

    // InvitationRevoke

    public interface InvitationRevokeCallback {
        void callback(Gsa2Exception exception);
    }

    public void invitationRevoke(String invitationId, String domainId, String access, InvitationRevokeCallback callback) {
        _invitationRevoke(invitationId, domainId, access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _invitationRevoke(String invitationId, String domainId, String access, InvitationRevokeCallback callback);

    // InvitationAccept

    public interface InvitationAcceptCallback {
        void callback(Gsa2DomainFull domainFull, Gsa2Exception exception);
    }

    public void invitationAccept(String invitationId, String access, InvitationAcceptCallback callback) {
        _invitationAccept(invitationId, access, (domainFull, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(domainFull, exception);
            });
        });
    }

    private native void _invitationAccept(String invitationId, String access, InvitationAcceptCallback callback);

    // CodeGet

    public interface CodeGetCallback {
        void callback(String code, Gsa2Exception exception);
    }

    public void codeGet(String domainId, String access, CodeGetCallback callback) {
        _codeGet(domainId, access, (code, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(code, exception);
            });
        });
    }

    private native void _codeGet(String domainId, String access, CodeGetCallback callback);

    // ControllersAdd

    public interface ControllersAddCallback {
        void callback(Gsa2ControllerFull[] controllersFull, Gsa2Exception exception);
    }

    public void controllersAdd(Gsa2ControllerFull[] controllersFull, String domainId, String access, ControllersAddCallback callback) {
        _controllersAdd(controllersFull, domainId, access, (_controllersFull, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(_controllersFull, exception);
            });
        });
    }

    private native void _controllersAdd(Gsa2ControllerFull[] controllersFull, String domainId, String access, ControllersAddCallback callback);

    // ControllerRemove

    public interface ControllerRemoveCallback {
        void callback(Gsa2Exception exception);
    }

    public void controllerRemove(String controllerId, String domainId, String access, ControllerRemoveCallback callback) {
        _controllerRemove(controllerId, domainId, access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _controllerRemove(String controllerId, String domainId, String access, ControllerRemoveCallback callback);

    // TokenIssue

    public interface TokenIssueCallback {
        void callback(Gsa2Token token, Gsa2Exception exception);
    }

    public void tokenIssue(Gsa2Identity identity, Gsa2Credential credential, TokenIssueCallback callback) {
        _tokenIssue(identity, credential, (token, exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(token, exception);
            });
        });
    }

    private native void _tokenIssue(Gsa2Identity identity, Gsa2Credential credential, TokenIssueCallback callback);

    // TokenRevoke

    public interface TokenRevokeCallback {
        void callback(Gsa2Exception exception);
    }

    public void tokenRevoke(String access, TokenRevokeCallback callback) {
        _tokenRevoke(access, (exception) -> {
            new Handler(Looper.getMainLooper()).post(() -> {
                callback.callback(exception);
            });
        });
    }

    private native void _tokenRevoke(String access, TokenRevokeCallback callback);
}
