package library.gsauth2;

import library.java.lang.LJLSDKException;

public class Gsa2Exception extends LJLSDKException {
    public Gsa2Exception(String domain, int code, String message) {
        super(domain, code, message);
    }
}
