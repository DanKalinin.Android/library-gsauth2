package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2DomainController extends LJLObject implements Serializable {
    public String domain;
    public String controller;
    public String key;
    public String ip;
    public int port;
    public String bssid;
    public boolean verified;
}
