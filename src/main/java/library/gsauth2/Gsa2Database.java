package library.gsauth2;

import library.java.lang.LJLObject;

public class Gsa2Database extends LJLObject {
    public static String FILE = "gs-auth-2.sqlite3";

    private long object;

    public static native Gsa2Database database(String file) throws Gsa2Exception;

    @Override
    protected native void finalize() throws Throwable;

    // Account

    public native Gsa2Account[] accountSelectById(String id) throws Gsa2Exception;
    public native void accountDeleteById(String id) throws Gsa2Exception;

    // Invitation

    public native Gsa2Invitation[] invitationSelectById(String id) throws Gsa2Exception;
    public native Gsa2Invitation[] invitationSelectByDomain(String domain, String tail) throws Gsa2Exception;

    // Token

    public native Gsa2Token[] tokenSelectBySubjectAndId(String subject, String id) throws Gsa2Exception;

    // AccountFull

    public native Gsa2AccountFull[] guestFullSelectByDomain(String domain, String tail) throws Gsa2Exception;
    public native Gsa2AccountFull[] guestFullSelectByDomainAndGuest(String domain, String guest) throws Gsa2Exception;

    // DomainFull

    public native Gsa2DomainFull[] domainFullSelectByAccount(String account, String tail) throws Gsa2Exception;
    public native Gsa2DomainFull[] domainFullSelectByAccountAndDomain(String account, String domain) throws Gsa2Exception;

    // ControllerFull

    public native Gsa2ControllerFull[] controllerFullSelectByDomain(String domain, String tail) throws Gsa2Exception;
    public native Gsa2ControllerFull[] controllerFullSelectByDomainAndController(String domain, String controller) throws Gsa2Exception;
}
