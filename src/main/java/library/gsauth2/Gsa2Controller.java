package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2Controller extends LJLObject implements Serializable {
    public String id;
    public String mac;
    public String model;
    public String serial;
}
