package library.gsauth2;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import library.android.LAInitializer;
import library.java.LJInitializer;
import library.java.lang.LJLObject;

public class Gsa2Initializer extends LJLObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-gsauth2");
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        ret.add(LJInitializer.class);
        ret.add(LAInitializer.class);
        return ret;
    }
}
