package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2Domain extends LJLObject implements Serializable {
    public String id;
    public String owner;
    public String name;
}
