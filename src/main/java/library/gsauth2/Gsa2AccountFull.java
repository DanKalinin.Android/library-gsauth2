package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2AccountFull extends LJLObject implements Serializable {
    public Gsa2Account account;
    public Gsa2AccountDomain accountDomain;
}
