package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2Identity extends LJLObject implements Serializable {
    public static String TYPE_PHONE;
    public static String TYPE_EMAIL;
    public static String TYPE_MAC;

    public String type;
    public String value;
    public String account;
    public String controller;
}
