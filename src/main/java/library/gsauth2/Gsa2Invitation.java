package library.gsauth2;

import java.io.Serializable;
import java.time.Instant;
import library.java.lang.LJLObject;

public class Gsa2Invitation extends LJLObject implements Serializable {
    public String id;
    public String domain;
    public String name;
    public Instant expiration;
    public long access;
}
