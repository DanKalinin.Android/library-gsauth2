package library.gsauth2;

import java.io.Serializable;
import java.time.Instant;
import library.java.lang.LJLObject;

public class Gsa2AccountDomain extends LJLObject implements Serializable {
    public String account;
    public String domain;
    public Instant expiration;
    public boolean current;
}
