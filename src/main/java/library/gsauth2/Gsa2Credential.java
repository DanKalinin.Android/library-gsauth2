package library.gsauth2;

import java.io.Serializable;
import library.java.lang.LJLObject;

public class Gsa2Credential extends LJLObject implements Serializable {
    public static String TYPE_PASSWORD;
    public static String TYPE_CODE;
    public static String TYPE_SERIAL;

    public String type;
    public String value;
}
