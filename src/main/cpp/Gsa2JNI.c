//
// Created by Dan Kalinin on 9/9/20.
//

#include "Gsa2JNI.h"

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    Gsa2Main.vm = vm;

    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_2);

    Gsa2ExceptionLoad(env);
    Gsa2AccountLoad(env);
    Gsa2DomainLoad(env);
    Gsa2ControllerLoad(env);
    Gsa2AccountDomainLoad(env);
    Gsa2DomainControllerLoad(env);
    Gsa2IdentityLoad(env);
    Gsa2InvitationLoad(env);
    Gsa2TokenLoad(env);
    Gsa2CredentialLoad(env);
    Gsa2AccountFullLoad(env);
    Gsa2DomainFullLoad(env);
    Gsa2ControllerFullLoad(env);
    Gsa2DatabaseLoad(env);
    Gsa2ClientLoad(env);

    gsa2_sdk_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_2);

    Gsa2ExceptionUnload(env);
    Gsa2AccountUnload(env);
    Gsa2DomainUnload(env);
    Gsa2ControllerUnload(env);
    Gsa2AccountDomainUnload(env);
    Gsa2DomainControllerUnload(env);
    Gsa2IdentityUnload(env);
    Gsa2InvitationUnload(env);
    Gsa2TokenUnload(env);
    Gsa2CredentialUnload(env);
    Gsa2AccountFullUnload(env);
    Gsa2DomainFullUnload(env);
    Gsa2ControllerFullUnload(env);
    Gsa2DatabaseUnload(env);
    Gsa2ClientUnload(env);
}
