//
// Created by Dan Kalinin on 9/5/20.
//

#include "Gsa2Database.h"

Gsa2DatabaseType Gsa2Database = {0};

void Gsa2DatabaseLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Database");
    Gsa2Database.class = (*env)->NewGlobalRef(env, class);
    Gsa2Database.object = (*env)->GetFieldID(env, class, "object", "J");
    Gsa2Database.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2DatabaseUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Database.class);
}

JNIEXPORT jobject JNICALL Java_library_gsauth2_Gsa2Database_database(JNIEnv *env, jclass class, jstring file) {
    jobject this = NULL;
    char *sdkFile = LJJNIEnvGetStringUTFChars(env, file, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKDatabase *object = gsa2_sdk_database_new(sdkFile, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, file, sdkFile);

    if (object == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        this = (*env)->NewObject(env, class, Gsa2Database.init);
        (*env)->SetLongField(env, this, Gsa2Database.object, (jlong)object);
    }

    return this;
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Database_finalize(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    gsa2_sdk_database_free((GSA2SDKDatabase *)object);
}

// Account

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_accountSelectById(JNIEnv *env, jobject this, jstring id) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkId = LJJNIEnvGetStringUTFChars(env, id, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKAccounts *sdkRet = gsa2_sdk_database_account_select_by_id((GSA2SDKDatabase *)object, sdkId, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, id, sdkId);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2AccountsFrom(env, sdkRet);
        gsa2_sdk_accounts_free(sdkRet);
    }

    return ret;
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Database_accountDeleteById(JNIEnv *env, jobject this, jstring id) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkId = LJJNIEnvGetStringUTFChars(env, id, NULL);
    GSA2SDKError *sdkError = NULL;
    jboolean ret = gsa2_sdk_database_account_delete_by_id((GSA2SDKDatabase *)object, sdkId, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, id, sdkId);

    if (!ret) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    }
}

// Invitation

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_invitationSelectById(JNIEnv *env, jobject this, jstring id) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkId = LJJNIEnvGetStringUTFChars(env, id, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKInvitations *sdkRet = gsa2_sdk_database_invitation_select_by_id((GSA2SDKDatabase *)object, sdkId, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, id, sdkId);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2InvitationsFrom(env, sdkRet);
        gsa2_sdk_invitations_free(sdkRet);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_invitationSelectByDomain(JNIEnv *env, jobject this, jstring domain, jstring tail) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkTail = LJJNIEnvGetStringUTFChars(env, tail, NULL);
    char *sdkDomain = LJJNIEnvGetStringUTFChars(env, domain, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKInvitations *sdkRet = gsa2_sdk_database_invitation_select_by_domain((GSA2SDKDatabase *)object, sdkTail, sdkDomain, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, tail, sdkTail);
    LJJNIEnvReleaseStringUTFChars(env, domain, sdkDomain);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2InvitationsFrom(env, sdkRet);
        gsa2_sdk_invitations_free(sdkRet);
    }

    return ret;
}

// Token

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_tokenSelectBySubjectAndId(JNIEnv *env, jobject this, jstring subject, jstring id) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkSubject = LJJNIEnvGetStringUTFChars(env, subject, NULL);
    char *sdkId = LJJNIEnvGetStringUTFChars(env, id, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKTokens *sdkRet = gsa2_sdk_database_token_select_by_subject_and_id((GSA2SDKDatabase *)object, sdkSubject, sdkId, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, subject, sdkSubject);
    LJJNIEnvReleaseStringUTFChars(env, id, sdkId);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2TokensFrom(env, sdkRet);
        gsa2_sdk_tokens_free(sdkRet);
    }

    return ret;
}

// AccountFull

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_guestFullSelectByDomain(JNIEnv *env, jobject this, jstring domain, jstring tail) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkTail = LJJNIEnvGetStringUTFChars(env, tail, NULL);
    char *sdkDomain = LJJNIEnvGetStringUTFChars(env, domain, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKAccountsFull *sdkRet = gsa2_sdk_database_guest_full_select_by_domain((GSA2SDKDatabase *)object, sdkTail, sdkDomain, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, tail, sdkTail);
    LJJNIEnvReleaseStringUTFChars(env, domain, sdkDomain);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2AccountsFullFrom(env, sdkRet);
        gsa2_sdk_accounts_full_free(sdkRet);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_guestFullSelectByDomainAndGuest(JNIEnv *env, jobject this, jstring domain, jstring guest) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkDomain = LJJNIEnvGetStringUTFChars(env, domain, NULL);
    char *sdkGuest = LJJNIEnvGetStringUTFChars(env, guest, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKAccountsFull *sdkRet = gsa2_sdk_database_guest_full_select_by_domain_and_guest((GSA2SDKDatabase *)object, sdkDomain, sdkGuest, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, domain, sdkDomain);
    LJJNIEnvReleaseStringUTFChars(env, guest, sdkGuest);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2AccountsFullFrom(env, sdkRet);
        gsa2_sdk_accounts_full_free(sdkRet);
    }

    return ret;
}

// DomainFull

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_domainFullSelectByAccount(JNIEnv *env, jobject this, jstring account, jstring tail) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkTail = LJJNIEnvGetStringUTFChars(env, tail, NULL);
    char *sdkAccount = LJJNIEnvGetStringUTFChars(env, account, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKDomainsFull *sdkRet = gsa2_sdk_database_domain_full_select_by_account((GSA2SDKDatabase *)object, sdkTail, sdkAccount, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, tail, sdkTail);
    LJJNIEnvReleaseStringUTFChars(env, account, sdkAccount);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2DomainsFullFrom(env, sdkRet);
        gsa2_sdk_domains_full_free(sdkRet);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_domainFullSelectByAccountAndDomain(JNIEnv *env, jobject this, jstring account, jstring domain) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkAccount = LJJNIEnvGetStringUTFChars(env, account, NULL);
    char *sdkDomain = LJJNIEnvGetStringUTFChars(env, domain, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKDomainsFull *sdkRet = gsa2_sdk_database_domain_full_select_by_account_and_domain((GSA2SDKDatabase *)object, sdkAccount, sdkDomain, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, account, sdkAccount);
    LJJNIEnvReleaseStringUTFChars(env, domain, sdkDomain);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2DomainsFullFrom(env, sdkRet);
        gsa2_sdk_domains_full_free(sdkRet);
    }

    return ret;
}

// ControllerFull

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_controllerFullSelectByDomain(JNIEnv *env, jobject this, jstring domain, jstring tail) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkTail = LJJNIEnvGetStringUTFChars(env, tail, NULL);
    char *sdkDomain = LJJNIEnvGetStringUTFChars(env, domain, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKControllersFull *sdkRet = gsa2_sdk_database_controller_full_select_by_domain((GSA2SDKDatabase *)object, sdkTail, sdkDomain, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, tail, sdkTail);
    LJJNIEnvReleaseStringUTFChars(env, domain, sdkDomain);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2ControllersFullFrom(env, sdkRet);
        gsa2_sdk_controllers_full_free(sdkRet);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_gsauth2_Gsa2Database_controllerFullSelectByDomainAndController(JNIEnv *env, jobject this, jstring domain, jstring controller) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, Gsa2Database.object);
    char *sdkDomain = LJJNIEnvGetStringUTFChars(env, domain, NULL);
    char *sdkController = LJJNIEnvGetStringUTFChars(env, controller, NULL);
    GSA2SDKError *sdkError = NULL;
    GSA2SDKControllersFull *sdkRet = gsa2_sdk_database_controller_full_select_by_domain_and_controller((GSA2SDKDatabase *)object, sdkDomain, sdkController, &sdkError);
    LJJNIEnvReleaseStringUTFChars(env, domain, sdkDomain);
    LJJNIEnvReleaseStringUTFChars(env, controller, sdkController);

    if (sdkRet == NULL) {
        jthrowable exception = Gsa2ExceptionFrom(env, sdkError);
        gsa2_sdk_error_free(sdkError);
        (*env)->Throw(env, exception);
    } else {
        ret = Gsa2ControllersFullFrom(env, sdkRet);
        gsa2_sdk_controllers_full_free(sdkRet);
    }

    return ret;
}
