//
// Created by Dan Kalinin on 9/2/20.
//

#ifndef LIBRARY_GSAUTH2_GSA2MAIN_H
#define LIBRARY_GSAUTH2_GSA2MAIN_H

#include <LibraryJava.h>
#include <LibraryAndroid.h>
#include "gsa2sdk.h"

typedef struct {
    JavaVM *vm;
} Gsa2MainType;

extern Gsa2MainType Gsa2Main;

#endif //LIBRARY_GSAUTH2_GSA2MAIN_H
