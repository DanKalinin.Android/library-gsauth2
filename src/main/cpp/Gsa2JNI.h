//
// Created by Dan Kalinin on 9/9/20.
//

#ifndef LIBRARY_GSAUTH2_GSA2JNI_H
#define LIBRARY_GSAUTH2_GSA2JNI_H

#include "Gsa2Main.h"
#include "Gsa2Exception.h"
#include "Gsa2Schema.h"
#include "Gsa2Database.h"
#include "Gsa2Client.h"

#endif //LIBRARY_GSAUTH2_GSA2JNI_H
