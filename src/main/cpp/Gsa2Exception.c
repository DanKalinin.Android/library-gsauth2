//
// Created by Dan Kalinin on 9/5/20.
//

#include "Gsa2Exception.h"
#include "Gsa2JNI.h"

Gsa2ExceptionType Gsa2Exception = {0};

void Gsa2ExceptionLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Exception");
    Gsa2Exception.class = (*env)->NewGlobalRef(env, class);
    Gsa2Exception.init = (*env)->GetMethodID(env, class, "<init>", "(Ljava/lang/String;ILjava/lang/String;)V");
}

void Gsa2ExceptionUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Exception.class);
}

jthrowable Gsa2ExceptionFrom(JNIEnv *env, GSA2SDKError *sdkError) {
    jstring domain = (*env)->NewStringUTF(env, sdkError->domain);
    jstring message = (*env)->NewStringUTF(env, sdkError->message);
    jobject this = (*env)->NewObject(env, Gsa2Exception.class, Gsa2Exception.init, domain, sdkError->code, message);
    return this;
}
