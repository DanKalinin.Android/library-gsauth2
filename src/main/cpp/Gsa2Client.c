//
// Created by Dan Kalinin on 9/5/20.
//

#include "Gsa2Client.h"
#include "Gsa2JNI.h"

Gsa2ClientType Gsa2Client = {0};

void Gsa2ClientLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client");
    Gsa2Client.class = (*env)->NewGlobalRef(env, class);
    Gsa2Client.object = (*env)->GetFieldID(env, class, "object", "J");
    Gsa2Client.init = (*env)->GetMethodID(env, class, "<init>", "()V");

    Gsa2ClientIdentityCheckCallbackLoad(env);
    Gsa2ClientCodeSendCallbackLoad(env);
    Gsa2ClientAccountCreateCallbackLoad(env);
    Gsa2ClientAccountGetCallbackLoad(env);
    Gsa2ClientPasswordUpdateCallbackLoad(env);
    Gsa2ClientDomainCreateCallbackLoad(env);
    Gsa2ClientDomainRenameCallbackLoad(env);
    Gsa2ClientDomainDeleteCallbackLoad(env);
    Gsa2ClientDomainExitCallbackLoad(env);
    Gsa2ClientGuestDeleteCallbackLoad(env);
    Gsa2ClientInvitationCreateCallbackLoad(env);
    Gsa2ClientInvitationRevokeCallbackLoad(env);
    Gsa2ClientInvitationAcceptCallbackLoad(env);
    Gsa2ClientCodeGetCallbackLoad(env);
    Gsa2ClientControllersAddCallbackLoad(env);
    Gsa2ClientControllerRemoveCallbackLoad(env);
    Gsa2ClientTokenIssueCallbackLoad(env);
    Gsa2ClientTokenRevokeCallbackLoad(env);
}

void Gsa2ClientUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Client.class);

    Gsa2ClientIdentityCheckCallbackUnload(env);
    Gsa2ClientCodeSendCallbackUnload(env);
    Gsa2ClientAccountCreateCallbackUnload(env);
    Gsa2ClientAccountGetCallbackUnload(env);
    Gsa2ClientPasswordUpdateCallbackUnload(env);
    Gsa2ClientDomainCreateCallbackUnload(env);
    Gsa2ClientDomainRenameCallbackUnload(env);
    Gsa2ClientDomainDeleteCallbackUnload(env);
    Gsa2ClientDomainExitCallbackUnload(env);
    Gsa2ClientGuestDeleteCallbackUnload(env);
    Gsa2ClientInvitationCreateCallbackUnload(env);
    Gsa2ClientInvitationRevokeCallbackUnload(env);
    Gsa2ClientInvitationAcceptCallbackUnload(env);
    Gsa2ClientCodeGetCallbackUnload(env);
    Gsa2ClientControllersAddCallbackUnload(env);
    Gsa2ClientControllerRemoveCallbackUnload(env);
    Gsa2ClientTokenIssueCallbackUnload(env);
    Gsa2ClientTokenRevokeCallbackUnload(env);
}

JNIEXPORT jobject JNICALL Java_library_gsauth2_Gsa2Client_client(JNIEnv *env, jclass class, jstring base, jobject database) {
    jobject this = (*env)->NewObject(env, class, Gsa2Client.init);
    char *sdkBase = LJJNIEnvGetStringUTFChars(env, base, NULL);
    jlong sdkDatabase = (*env)->GetLongField(env, database, Gsa2Database.object);
    GSA2SDKClient *object = gsa2_sdk_client_new(sdkBase, (GSA2SDKDatabase *)sdkDatabase);
    LJJNIEnvReleaseStringUTFChars(env, base, sdkBase);
    (*env)->SetLongField(env, this, Gsa2Client.object, (jlong)object);
    return this;
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client_finalize(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    gsa2_sdk_client_free((GSA2SDKClient *)object);
}

// IdentityCheck

Gsa2ClientIdentityCheckCallbackType Gsa2ClientIdentityCheckCallback = {0};

void Gsa2ClientIdentityCheckCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$IdentityCheckCallback");
    Gsa2ClientIdentityCheckCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientIdentityCheckCallback.callback = (*env)->GetMethodID(env, class, "callback", "(ZLlibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientIdentityCheckCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientIdentityCheckCallback.class);
}

void Gsa2ClientGSA2SDKClientIdentityCheckCallback(int available, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientIdentityCheckCallback.callback, available, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1identityCheck(JNIEnv *env, jobject this, jobject identity, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    GSA2Identity sdkIdentity = {0};
    Gsa2IdentityTo(env, identity, &sdkIdentity);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_identity_check((GSA2SDKClient *)object, &sdkIdentity, Gsa2ClientGSA2SDKClientIdentityCheckCallback, callback);
    Gsa2IdentityRelease(env, identity, &sdkIdentity);
}

// CodeSend

Gsa2ClientCodeSendCallbackType Gsa2ClientCodeSendCallback = {0};

void Gsa2ClientCodeSendCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$CodeSendCallback");
    Gsa2ClientCodeSendCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientCodeSendCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Ljava/time/Instant;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientCodeSendCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientCodeSendCallback.class);
}

void Gsa2ClientGSA2SDKClientCodeSendCallback(long sdkExpiration, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject expiration = NULL;
    jthrowable exception = NULL;

    if (sdkError == NULL) {
        expiration = (*env)->CallStaticObjectMethod(env, JTInstant.class, JTInstant.ofEpochSecond, sdkExpiration);
    } else {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientCodeSendCallback.callback, expiration, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1codeSend(JNIEnv *env, jobject this, jobject identity, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    GSA2Identity sdkIdentity = {0};
    Gsa2IdentityTo(env, identity, &sdkIdentity);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_code_send((GSA2SDKClient *)object, &sdkIdentity, Gsa2ClientGSA2SDKClientCodeSendCallback, callback);
    Gsa2IdentityRelease(env, identity, &sdkIdentity);
}

// AccountCreate

Gsa2ClientAccountCreateCallbackType Gsa2ClientAccountCreateCallback = {0};

void Gsa2ClientAccountCreateCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$AccountCreateCallback");
    Gsa2ClientAccountCreateCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientAccountCreateCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Account;Ljava/time/Instant;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientAccountCreateCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientAccountCreateCallback.class);
}

void Gsa2ClientGSA2SDKClientAccountCreateCallback(GSA2Account *sdkAccount, long sdkExpiration, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject account = NULL;
    jobject expiration = NULL;
    jthrowable exception = NULL;

    if (sdkAccount == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        account = Gsa2AccountFrom(env, sdkAccount);
        expiration = (*env)->CallStaticObjectMethod(env, JTInstant.class, JTInstant.ofEpochSecond, sdkExpiration);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientAccountCreateCallback.callback, account, expiration, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1accountCreate(JNIEnv *env, jobject this, jobject account, jobjectArray identities, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    GSA2Account sdkAccount = {0};
    Gsa2AccountTo(env, account, &sdkAccount);
    jsize identitiesN = LJJNIEnvGetArrayLength(env, identities);
    GSA2Identity *sdkIdentitiesData[identitiesN];
    GSA2Identity sdkIdentitiesValue[identitiesN];

    for (jsize i = 0; i < identitiesN; i++) {
        sdkIdentitiesData[i] = &sdkIdentitiesValue[i];
    }

    GSA2SDKIdentities sdkIdentities = {0};
    sdkIdentities.data = sdkIdentitiesData;
    sdkIdentities.n = identitiesN;
    Gsa2IdentitiesTo(env, identities, &sdkIdentities);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_account_create((GSA2SDKClient *)object, &sdkAccount, &sdkIdentities, Gsa2ClientGSA2SDKClientAccountCreateCallback, callback);
    Gsa2AccountRelease(env, account, &sdkAccount);
    Gsa2IdentitiesRelease(env, identities, &sdkIdentities);
}

// AccountGet

Gsa2ClientAccountGetCallbackType Gsa2ClientAccountGetCallback = {0};

void Gsa2ClientAccountGetCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$AccountGetCallback");
    Gsa2ClientAccountGetCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientAccountGetCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Account;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientAccountGetCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientAccountGetCallback.class);
}

void Gsa2ClientGSA2SDKClientAccountGetCallback(GSA2Account *sdkAccount, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject account = NULL;
    jthrowable exception = NULL;

    if (sdkAccount == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        account = Gsa2AccountFrom(env, sdkAccount);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientAccountGetCallback.callback, account, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1accountGet(JNIEnv *env, jobject this, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_account_get((GSA2SDKClient *)object, sdkAccess, Gsa2ClientGSA2SDKClientAccountGetCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
}

// PasswordUpdate

Gsa2ClientPasswordUpdateCallbackType Gsa2ClientPasswordUpdateCallback = {0};

void Gsa2ClientPasswordUpdateCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$PasswordUpdateCallback");
    Gsa2ClientPasswordUpdateCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientPasswordUpdateCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientPasswordUpdateCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientPasswordUpdateCallback.class);
}

void Gsa2ClientGSA2SDKClientPasswordUpdateCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientPasswordUpdateCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1passwordUpdate(JNIEnv *env, jobject this, jstring password, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkPassword = LJJNIEnvGetStringUTFChars(env, password, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_password_update((GSA2SDKClient *)object, sdkAccess, sdkPassword, Gsa2ClientGSA2SDKClientPasswordUpdateCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, password, sdkPassword);
}

// DomainCreate

Gsa2ClientDomainCreateCallbackType Gsa2ClientDomainCreateCallback = {0};

void Gsa2ClientDomainCreateCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$DomainCreateCallback");
    Gsa2ClientDomainCreateCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientDomainCreateCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2DomainFull;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientDomainCreateCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientDomainCreateCallback.class);
}

void Gsa2ClientGSA2SDKClientDomainCreateCallback(GSA2DomainFull *sdkDomainFull, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject domainFull = NULL;
    jthrowable exception = NULL;

    if (sdkDomainFull == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        domainFull = Gsa2DomainFullFrom(env, sdkDomainFull);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientDomainCreateCallback.callback, domainFull, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1domainCreate(JNIEnv *env, jobject this, jobject domain, jobjectArray controllersFull, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    GSA2Domain sdkDomain = {0};
    Gsa2DomainTo(env, domain, &sdkDomain);
    jsize controllersFullN = LJJNIEnvGetArrayLength(env, controllersFull);
    GSA2ControllerFull *sdkControllersFullData[controllersFullN];
    GSA2ControllerFull sdkControllersFullValue[controllersFullN];

    for (jsize i = 0; i < controllersFullN; i++) {
        sdkControllersFullData[i] = &sdkControllersFullValue[i];
    }

    GSA2SDKControllersFull sdkControllersFull = {0};
    sdkControllersFull.data = sdkControllersFullData;
    sdkControllersFull.n = controllersFullN;
    Gsa2ControllersFullTo(env, controllersFull, &sdkControllersFull);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_domain_create((GSA2SDKClient *)object, sdkAccess, &sdkDomain, &sdkControllersFull, Gsa2ClientGSA2SDKClientDomainCreateCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    Gsa2DomainRelease(env, domain, &sdkDomain);
    Gsa2ControllersFullRelease(env, controllersFull, &sdkControllersFull);
}

// DomainRename

Gsa2ClientDomainRenameCallbackType Gsa2ClientDomainRenameCallback = {0};

void Gsa2ClientDomainRenameCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$DomainRenameCallback");
    Gsa2ClientDomainRenameCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientDomainRenameCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientDomainRenameCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientDomainRenameCallback.class);
}

void Gsa2ClientGSA2SDKClientDomainRenameCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientDomainRenameCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1domainRename(JNIEnv *env, jobject this, jstring domainId, jstring name, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    char *sdkName = LJJNIEnvGetStringUTFChars(env, name, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_domain_rename((GSA2SDKClient *)object, sdkAccess, sdkDomainId, sdkName, Gsa2ClientGSA2SDKClientDomainRenameCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
    LJJNIEnvReleaseStringUTFChars(env, name, sdkName);
}

// DomainDelete

Gsa2ClientDomainDeleteCallbackType Gsa2ClientDomainDeleteCallback = {0};

void Gsa2ClientDomainDeleteCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$DomainDeleteCallback");
    Gsa2ClientDomainDeleteCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientDomainDeleteCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientDomainDeleteCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientDomainDeleteCallback.class);
}

void Gsa2ClientGSA2SDKClientDomainDeleteCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientDomainDeleteCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1domainDelete(JNIEnv *env, jobject this, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_domain_delete((GSA2SDKClient *)object, sdkAccess, sdkDomainId, Gsa2ClientGSA2SDKClientDomainDeleteCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
}

// DomainExit

Gsa2ClientDomainExitCallbackType Gsa2ClientDomainExitCallback = {0};

void Gsa2ClientDomainExitCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$DomainExitCallback");
    Gsa2ClientDomainExitCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientDomainExitCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientDomainExitCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientDomainExitCallback.class);
}

void Gsa2ClientGSA2SDKClientDomainExitCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientDomainExitCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1domainExit(JNIEnv *env, jobject this, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_domain_exit((GSA2SDKClient *)object, sdkAccess, sdkDomainId, Gsa2ClientGSA2SDKClientDomainExitCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
}

// GuestDelete

Gsa2ClientGuestDeleteCallbackType Gsa2ClientGuestDeleteCallback = {0};

void Gsa2ClientGuestDeleteCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$GuestDeleteCallback");
    Gsa2ClientGuestDeleteCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientGuestDeleteCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2DomainFull;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientGuestDeleteCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientGuestDeleteCallback.class);
}

void Gsa2ClientGSA2SDKClientGuestDeleteCallback(GSA2DomainFull *sdkDomainFull, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject domainFull = NULL;
    jthrowable exception = NULL;

    if (sdkDomainFull == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        domainFull = Gsa2DomainFullFrom(env, sdkDomainFull);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientGuestDeleteCallback.callback, domainFull, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1guestDelete(JNIEnv *env, jobject this, jstring guestId, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    char *sdkGuestId = LJJNIEnvGetStringUTFChars(env, guestId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_guest_delete((GSA2SDKClient *)object, sdkAccess, sdkDomainId, sdkGuestId, Gsa2ClientGSA2SDKClientGuestDeleteCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
    LJJNIEnvReleaseStringUTFChars(env, guestId, sdkGuestId);
}

// InvitationCreate

Gsa2ClientInvitationCreateCallbackType Gsa2ClientInvitationCreateCallback = {0};

void Gsa2ClientInvitationCreateCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$InvitationCreateCallback");
    Gsa2ClientInvitationCreateCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientInvitationCreateCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Invitation;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientInvitationCreateCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientInvitationCreateCallback.class);
}

void Gsa2ClientGSA2SDKClientInvitationCreateCallback(GSA2Invitation *sdkInvitation, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject invitation = NULL;
    jthrowable exception = NULL;

    if (sdkInvitation == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        invitation = Gsa2InvitationFrom(env, sdkInvitation);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientInvitationCreateCallback.callback, invitation, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1invitationCreate(JNIEnv *env, jobject this, jobject invitation, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    GSA2Invitation sdkInvitation = {0};
    Gsa2InvitationTo(env, invitation, &sdkInvitation);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_invitation_create((GSA2SDKClient *)object, sdkAccess, sdkDomainId, &sdkInvitation, Gsa2ClientGSA2SDKClientInvitationCreateCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
    Gsa2InvitationRelease(env, invitation, &sdkInvitation);
}

// InvitationRevoke

Gsa2ClientInvitationRevokeCallbackType Gsa2ClientInvitationRevokeCallback = {0};

void Gsa2ClientInvitationRevokeCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$InvitationRevokeCallback");
    Gsa2ClientInvitationRevokeCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientInvitationRevokeCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientInvitationRevokeCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientInvitationRevokeCallback.class);
}

void Gsa2ClientGSA2SDKClientInvitationRevokeCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientInvitationRevokeCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1invitationRevoke(JNIEnv *env, jobject this, jstring invitationId, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    char *sdkInvitationId = LJJNIEnvGetStringUTFChars(env, invitationId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_invitation_revoke((GSA2SDKClient *)object, sdkAccess, sdkDomainId, sdkInvitationId, Gsa2ClientGSA2SDKClientInvitationRevokeCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
    LJJNIEnvReleaseStringUTFChars(env, invitationId, sdkInvitationId);
}

// InvitationAccept

Gsa2ClientInvitationAcceptCallbackType Gsa2ClientInvitationAcceptCallback = {0};

void Gsa2ClientInvitationAcceptCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$InvitationAcceptCallback");
    Gsa2ClientInvitationAcceptCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientInvitationAcceptCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2DomainFull;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientInvitationAcceptCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientInvitationAcceptCallback.class);
}

void Gsa2ClientGSA2SDKClientInvitationAcceptCallback(GSA2DomainFull *sdkDomainFull, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject domainFull = NULL;
    jthrowable exception = NULL;

    if (sdkDomainFull == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        domainFull = Gsa2DomainFullFrom(env, sdkDomainFull);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientInvitationAcceptCallback.callback, domainFull, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1invitationAccept(JNIEnv *env, jobject this, jstring invitationId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkInvitationId = LJJNIEnvGetStringUTFChars(env, invitationId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_invitation_accept((GSA2SDKClient *)object, sdkAccess, sdkInvitationId, Gsa2ClientGSA2SDKClientInvitationAcceptCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, invitationId, sdkInvitationId);
}

// CodeGet

Gsa2ClientCodeGetCallbackType Gsa2ClientCodeGetCallback = {0};

void Gsa2ClientCodeGetCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$CodeGetCallback");
    Gsa2ClientCodeGetCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientCodeGetCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Ljava/lang/String;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientCodeGetCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientCodeGetCallback.class);
}

void Gsa2ClientGSA2SDKClientCodeGetCallback(char *sdkCode, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jstring code = NULL;
    jthrowable exception = NULL;

    if (sdkCode == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        code = (*env)->NewStringUTF(env, sdkCode);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientCodeGetCallback.callback, code, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1codeGet(JNIEnv *env, jobject this, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_code_get((GSA2SDKClient *)object, sdkAccess, sdkDomainId, Gsa2ClientGSA2SDKClientCodeGetCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
}

// ControllersAdd

Gsa2ClientControllersAddCallbackType Gsa2ClientControllersAddCallback = {0};

void Gsa2ClientControllersAddCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$ControllersAddCallback");
    Gsa2ClientControllersAddCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientControllersAddCallback.callback = (*env)->GetMethodID(env, class, "callback", "([Llibrary/gsauth2/Gsa2ControllerFull;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientControllersAddCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientControllersAddCallback.class);
}

void Gsa2ClientGSA2SDKClientControllersAddCallback(GSA2SDKControllersFull *sdkControllersFull, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobjectArray controllersFull = NULL;
    jthrowable exception = NULL;

    if (sdkControllersFull == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        controllersFull = Gsa2ControllersFullFrom(env, sdkControllersFull);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientControllersAddCallback.callback, controllersFull, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1controllersAdd(JNIEnv *env, jobject this, jobjectArray controllersFull, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    jsize controllersFullN = LJJNIEnvGetArrayLength(env, controllersFull);
    GSA2ControllerFull *sdkControllersFullData[controllersFullN];
    GSA2ControllerFull sdkControllersFullValue[controllersFullN];

    for (jsize i = 0; i < controllersFullN; i++) {
        sdkControllersFullData[i] = &sdkControllersFullValue[i];
    }

    GSA2SDKControllersFull sdkControllersFull = {0};
    sdkControllersFull.data = sdkControllersFullData;
    sdkControllersFull.n = controllersFullN;
    Gsa2ControllersFullTo(env, controllersFull, &sdkControllersFull);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_controllers_add((GSA2SDKClient *)object, sdkAccess, sdkDomainId, &sdkControllersFull, Gsa2ClientGSA2SDKClientControllersAddCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
    Gsa2ControllersFullRelease(env, controllersFull, &sdkControllersFull);
}

// ControllerRemove

Gsa2ClientControllerRemoveCallbackType Gsa2ClientControllerRemoveCallback = {0};

void Gsa2ClientControllerRemoveCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$ControllerRemoveCallback");
    Gsa2ClientControllerRemoveCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientControllerRemoveCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientControllerRemoveCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientControllerRemoveCallback.class);
}

void Gsa2ClientGSA2SDKClientControllerRemoveCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientControllerRemoveCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1controllerRemove(JNIEnv *env, jobject this, jstring controllerId, jstring domainId, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    char *sdkDomainId = LJJNIEnvGetStringUTFChars(env, domainId, NULL);
    char *sdkControllerId = LJJNIEnvGetStringUTFChars(env, controllerId, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_controller_remove((GSA2SDKClient *)object, sdkAccess, sdkDomainId, sdkControllerId, Gsa2ClientGSA2SDKClientControllerRemoveCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
    LJJNIEnvReleaseStringUTFChars(env, domainId, sdkDomainId);
    LJJNIEnvReleaseStringUTFChars(env, controllerId, sdkControllerId);
}

// TokenIssue

Gsa2ClientTokenIssueCallbackType Gsa2ClientTokenIssueCallback = {0};

void Gsa2ClientTokenIssueCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$TokenIssueCallback");
    Gsa2ClientTokenIssueCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientTokenIssueCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Token;Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientTokenIssueCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientTokenIssueCallback.class);
}

void Gsa2ClientGSA2SDKClientTokenIssueCallback(GSA2Token *sdkToken, GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jobject token = NULL;
    jthrowable exception = NULL;

    if (sdkToken == NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    } else {
        token = Gsa2TokenFrom(env, sdkToken);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientTokenIssueCallback.callback, token, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1tokenIssue(JNIEnv *env, jobject this, jobject identity, jobject credential, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    GSA2Identity sdkIdentity = {0};
    Gsa2IdentityTo(env, identity, &sdkIdentity);
    GSA2Credential sdkCredential = {0};
    Gsa2CredentialTo(env, credential, &sdkCredential);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_token_issue((GSA2SDKClient *)object, &sdkIdentity, &sdkCredential, Gsa2ClientGSA2SDKClientTokenIssueCallback, callback);
    Gsa2IdentityRelease(env, identity, &sdkIdentity);
    Gsa2CredentialRelease(env, credential, &sdkCredential);
}

// TokenRevoke

Gsa2ClientTokenRevokeCallbackType Gsa2ClientTokenRevokeCallback = {0};

void Gsa2ClientTokenRevokeCallbackLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Client$TokenRevokeCallback");
    Gsa2ClientTokenRevokeCallback.class = (*env)->NewGlobalRef(env, class);
    Gsa2ClientTokenRevokeCallback.callback = (*env)->GetMethodID(env, class, "callback", "(Llibrary/gsauth2/Gsa2Exception;)V");
}

void Gsa2ClientTokenRevokeCallbackUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ClientTokenRevokeCallback.class);
}

void Gsa2ClientGSA2SDKClientTokenRevokeCallback(GSA2SDKError *sdkError, void *callback) {
    JNIEnv *env = NULL;
    (void)(*Gsa2Main.vm)->AttachCurrentThread(Gsa2Main.vm, &env, NULL);
    jthrowable exception = NULL;

    if (sdkError != NULL) {
        exception = Gsa2ExceptionFrom(env, sdkError);
    }

    LJJNIEnvCallVoidMethod(env, callback, Gsa2ClientTokenRevokeCallback.callback, exception);
    (*env)->DeleteGlobalRef(env, callback);
    (void)(*Gsa2Main.vm)->DetachCurrentThread(Gsa2Main.vm);
}

JNIEXPORT void JNICALL Java_library_gsauth2_Gsa2Client__1tokenRevoke(JNIEnv *env, jobject this, jstring access, jobject callback) {
    jlong object = (*env)->GetLongField(env, this, Gsa2Client.object);
    char *sdkAccess = LJJNIEnvGetStringUTFChars(env, access, NULL);
    callback = (*env)->NewGlobalRef(env, callback);
    gsa2_sdk_client_token_revoke((GSA2SDKClient *)object, sdkAccess, Gsa2ClientGSA2SDKClientTokenRevokeCallback, callback);
    LJJNIEnvReleaseStringUTFChars(env, access, sdkAccess);
}
