//
// Created by Dan Kalinin on 9/2/20.
//

#ifndef LIBRARY_GSAUTH2_GSAUTH2_H
#define LIBRARY_GSAUTH2_GSAUTH2_H

#include <Gsa2Main.h>
#include <Gsa2Exception.h>
#include <Gsa2Schema.h>
#include <Gsa2Database.h>
#include <Gsa2Client.h>
#include <Gsa2JNI.h>

#endif //LIBRARY_GSAUTH2_GSAUTH2_H
