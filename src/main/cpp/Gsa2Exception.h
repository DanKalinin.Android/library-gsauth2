//
// Created by Dan Kalinin on 9/5/20.
//

#ifndef LIBRARY_GSAUTH2_GSA2EXCEPTION_H
#define LIBRARY_GSAUTH2_GSA2EXCEPTION_H

#include "Gsa2Main.h"

typedef struct {
    jclass class;
    jmethodID init;
} Gsa2ExceptionType;

extern Gsa2ExceptionType Gsa2Exception;

void Gsa2ExceptionLoad(JNIEnv *env);
void Gsa2ExceptionUnload(JNIEnv *env);
jthrowable Gsa2ExceptionFrom(JNIEnv *env, GSA2SDKError *sdkError);

#endif //LIBRARY_GSAUTH2_GSA2EXCEPTION_H
