//
// Created by Dan Kalinin on 9/5/20.
//

#ifndef LIBRARY_GSAUTH2_GSA2DATABASE_H
#define LIBRARY_GSAUTH2_GSA2DATABASE_H

#include "Gsa2Main.h"
#include "Gsa2Schema.h"
#include "Gsa2Exception.h"

typedef struct {
    jclass class;
    jfieldID object;
    jmethodID init;
} Gsa2DatabaseType;

extern Gsa2DatabaseType Gsa2Database;

void Gsa2DatabaseLoad(JNIEnv *env);
void Gsa2DatabaseUnload(JNIEnv *env);

#endif //LIBRARY_GSAUTH2_GSA2DATABASE_H
