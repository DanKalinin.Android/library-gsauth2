//
// Created by Dan Kalinin on 9/5/20.
//

#include "Gsa2Schema.h"

// Account

Gsa2AccountType Gsa2Account = {0};

void Gsa2AccountLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Account");
    Gsa2Account.class = (*env)->NewGlobalRef(env, class);
    Gsa2Account.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    Gsa2Account.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    Gsa2Account.password = (*env)->GetFieldID(env, class, "password", "Ljava/lang/String;");
    Gsa2Account.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2AccountUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Account.class);
}

jobject Gsa2AccountFrom(JNIEnv *env, GSA2Account *account) {
    jobject this = (*env)->NewObject(env, Gsa2Account.class, Gsa2Account.init);

    jstring id = (*env)->NewStringUTF(env, account->id);
    (*env)->SetObjectField(env, this, Gsa2Account.id, id);

    jstring name = (*env)->NewStringUTF(env, account->name);
    (*env)->SetObjectField(env, this, Gsa2Account.name, name);

    jstring password = (*env)->NewStringUTF(env, account->password);
    (*env)->SetObjectField(env, this, Gsa2Account.password, password);

    return this;
}

void Gsa2AccountTo(JNIEnv *env, jobject this, GSA2Account *account) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Account.id);
    account->id = LJJNIEnvGetStringUTFChars(env, id, NULL);

    jstring name = (*env)->GetObjectField(env, this, Gsa2Account.name);
    account->name = LJJNIEnvGetStringUTFChars(env, name, NULL);

    jstring password = (*env)->GetObjectField(env, this, Gsa2Account.password);
    account->password = LJJNIEnvGetStringUTFChars(env, password, NULL);
}

void Gsa2AccountRelease(JNIEnv *env, jobject this, GSA2Account *account) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Account.id);
    LJJNIEnvReleaseStringUTFChars(env, id, account->id);

    jstring name = (*env)->GetObjectField(env, this, Gsa2Account.name);
    LJJNIEnvReleaseStringUTFChars(env, name, account->name);

    jstring password = (*env)->GetObjectField(env, this, Gsa2Account.password);
    LJJNIEnvReleaseStringUTFChars(env, password, account->password);
}

jobjectArray Gsa2AccountsFrom(JNIEnv *env, GSA2SDKAccounts *sdkAccounts) {
    jobjectArray this = (*env)->NewObjectArray(env, sdkAccounts->n, Gsa2Account.class, NULL);

    for (int i = 0; i < sdkAccounts->n; i++) {
        GSA2Account *sdkAccount = sdkAccounts->data[i];
        jobject account = Gsa2AccountFrom(env, sdkAccount);
        (*env)->SetObjectArrayElement(env, this, i, account);
        (*env)->DeleteLocalRef(env, account);
    }

    return this;
}

// Domain

Gsa2DomainType Gsa2Domain = {0};

void Gsa2DomainLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Domain");
    Gsa2Domain.class = (*env)->NewGlobalRef(env, class);
    Gsa2Domain.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    Gsa2Domain.owner = (*env)->GetFieldID(env, class, "owner", "Ljava/lang/String;");
    Gsa2Domain.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    Gsa2Domain.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2DomainUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Domain.class);
}

jobject Gsa2DomainFrom(JNIEnv *env, GSA2Domain *domain) {
    jobject this = (*env)->NewObject(env, Gsa2Domain.class, Gsa2Domain.init);

    jstring id = (*env)->NewStringUTF(env, domain->id);
    (*env)->SetObjectField(env, this, Gsa2Domain.id, id);

    jstring owner = (*env)->NewStringUTF(env, domain->owner);
    (*env)->SetObjectField(env, this, Gsa2Domain.owner, owner);

    jstring name = (*env)->NewStringUTF(env, domain->name);
    (*env)->SetObjectField(env, this, Gsa2Domain.name, name);

    return this;
}

void Gsa2DomainTo(JNIEnv *env, jobject this, GSA2Domain *domain) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Domain.id);
    domain->id = LJJNIEnvGetStringUTFChars(env, id, NULL);

    jstring owner = (*env)->GetObjectField(env, this, Gsa2Domain.owner);
    domain->owner = LJJNIEnvGetStringUTFChars(env, owner, NULL);

    jstring name = (*env)->GetObjectField(env, this, Gsa2Domain.name);
    domain->name = LJJNIEnvGetStringUTFChars(env, name, NULL);
}

void Gsa2DomainRelease(JNIEnv *env, jobject this, GSA2Domain *domain) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Domain.id);
    LJJNIEnvReleaseStringUTFChars(env, id, domain->id);

    jstring owner = (*env)->GetObjectField(env, this, Gsa2Domain.owner);
    LJJNIEnvReleaseStringUTFChars(env, owner, domain->owner);

    jstring name = (*env)->GetObjectField(env, this, Gsa2Domain.name);
    LJJNIEnvReleaseStringUTFChars(env, name, domain->name);
}

// Controller

Gsa2ControllerType Gsa2Controller = {0};

void Gsa2ControllerLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Controller");
    Gsa2Controller.class = (*env)->NewGlobalRef(env, class);
    Gsa2Controller.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    Gsa2Controller.mac = (*env)->GetFieldID(env, class, "mac", "Ljava/lang/String;");
    Gsa2Controller.model = (*env)->GetFieldID(env, class, "model", "Ljava/lang/String;");
    Gsa2Controller.serial = (*env)->GetFieldID(env, class, "serial", "Ljava/lang/String;");
    Gsa2Controller.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2ControllerUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Controller.class);
}

jobject Gsa2ControllerFrom(JNIEnv *env, GSA2Controller *controller) {
    jobject this = (*env)->NewObject(env, Gsa2Controller.class, Gsa2Controller.init);

    jstring id = (*env)->NewStringUTF(env, controller->id);
    (*env)->SetObjectField(env, this, Gsa2Controller.id, id);

    jstring mac = (*env)->NewStringUTF(env, controller->mac);
    (*env)->SetObjectField(env, this, Gsa2Controller.mac, mac);

    jstring model = (*env)->NewStringUTF(env, controller->model);
    (*env)->SetObjectField(env, this, Gsa2Controller.model, model);

    jstring serial = (*env)->NewStringUTF(env, controller->serial);
    (*env)->SetObjectField(env, this, Gsa2Controller.serial, serial);

    return this;
}

void Gsa2ControllerTo(JNIEnv *env, jobject this, GSA2Controller *controller) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Controller.id);
    controller->id = LJJNIEnvGetStringUTFChars(env, id, NULL);

    jstring mac = (*env)->GetObjectField(env, this, Gsa2Controller.mac);
    controller->mac = LJJNIEnvGetStringUTFChars(env, mac, NULL);

    jstring model = (*env)->GetObjectField(env, this, Gsa2Controller.model);
    controller->model = LJJNIEnvGetStringUTFChars(env, model, NULL);

    jstring serial = (*env)->GetObjectField(env, this, Gsa2Controller.serial);
    controller->serial = LJJNIEnvGetStringUTFChars(env, serial, NULL);
}

void Gsa2ControllerRelease(JNIEnv *env, jobject this, GSA2Controller *controller) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Controller.id);
    LJJNIEnvReleaseStringUTFChars(env, id, controller->id);

    jstring mac = (*env)->GetObjectField(env, this, Gsa2Controller.mac);
    LJJNIEnvReleaseStringUTFChars(env, mac, controller->mac);

    jstring model = (*env)->GetObjectField(env, this, Gsa2Controller.model);
    LJJNIEnvReleaseStringUTFChars(env, model, controller->model);

    jstring serial = (*env)->GetObjectField(env, this, Gsa2Controller.serial);
    LJJNIEnvReleaseStringUTFChars(env, serial, controller->serial);
}

// AccountDomain

Gsa2AccountDomainType Gsa2AccountDomain = {0};

void Gsa2AccountDomainLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2AccountDomain");
    Gsa2AccountDomain.class = (*env)->NewGlobalRef(env, class);
    Gsa2AccountDomain.account = (*env)->GetFieldID(env, class, "account", "Ljava/lang/String;");
    Gsa2AccountDomain.domain = (*env)->GetFieldID(env, class, "domain", "Ljava/lang/String;");
    Gsa2AccountDomain.expiration = (*env)->GetFieldID(env, class, "expiration", "Ljava/time/Instant;");
    Gsa2AccountDomain.current = (*env)->GetFieldID(env, class, "current", "Z");
    Gsa2AccountDomain.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2AccountDomainUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2AccountDomain.class);
}

jobject Gsa2AccountDomainFrom(JNIEnv *env, GSA2AccountDomain *accountDomain) {
    jobject this = (*env)->NewObject(env, Gsa2AccountDomain.class, Gsa2AccountDomain.init);

    jstring account = (*env)->NewStringUTF(env, accountDomain->account);
    (*env)->SetObjectField(env, this, Gsa2AccountDomain.account, account);

    jstring domain = (*env)->NewStringUTF(env, accountDomain->domain);
    (*env)->SetObjectField(env, this, Gsa2AccountDomain.domain, domain);

    jstring stringExpiration = (*env)->NewStringUTF(env, accountDomain->expiration);
    jobject expiration = (*env)->CallStaticObjectMethod(env, JTInstant.class, JTInstant.parse, stringExpiration);
    (*env)->ExceptionClear(env);
    (*env)->SetObjectField(env, this, Gsa2AccountDomain.expiration, expiration);

    (*env)->SetBooleanField(env, this, Gsa2AccountDomain.current, accountDomain->current);

    return this;
}

// DomainController

Gsa2DomainControllerType Gsa2DomainController = {0};

void Gsa2DomainControllerLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2DomainController");
    Gsa2DomainController.class = (*env)->NewGlobalRef(env, class);
    Gsa2DomainController.domain = (*env)->GetFieldID(env, class, "domain", "Ljava/lang/String;");
    Gsa2DomainController.controller = (*env)->GetFieldID(env, class, "controller", "Ljava/lang/String;");
    Gsa2DomainController.key = (*env)->GetFieldID(env, class, "key", "Ljava/lang/String;");
    Gsa2DomainController.ip = (*env)->GetFieldID(env, class, "ip", "Ljava/lang/String;");
    Gsa2DomainController.port = (*env)->GetFieldID(env, class, "port", "I");
    Gsa2DomainController.bssid = (*env)->GetFieldID(env, class, "bssid", "Ljava/lang/String;");
    Gsa2DomainController.verified = (*env)->GetFieldID(env, class, "verified", "Z");
    Gsa2DomainController.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2DomainControllerUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2DomainController.class);
}

jobject Gsa2DomainControllerFrom(JNIEnv *env, GSA2DomainController *domainController) {
    jobject this = (*env)->NewObject(env, Gsa2DomainController.class, Gsa2DomainController.init);

    jstring domain = (*env)->NewStringUTF(env, domainController->domain);
    (*env)->SetObjectField(env, this, Gsa2DomainController.domain, domain);

    jstring controller = (*env)->NewStringUTF(env, domainController->controller);
    (*env)->SetObjectField(env, this, Gsa2DomainController.controller, controller);

    jstring key = (*env)->NewStringUTF(env, domainController->key);
    (*env)->SetObjectField(env, this, Gsa2DomainController.key, key);

    jstring ip = (*env)->NewStringUTF(env, domainController->ip);
    (*env)->SetObjectField(env, this, Gsa2DomainController.ip, ip);

    (*env)->SetIntField(env, this, Gsa2DomainController.port, domainController->port);

    jstring bssid = (*env)->NewStringUTF(env, domainController->bssid);
    (*env)->SetObjectField(env, this, Gsa2DomainController.bssid, bssid);

    (*env)->SetBooleanField(env, this, Gsa2DomainController.verified, domainController->verified);

    return this;
}

void Gsa2DomainControllerTo(JNIEnv *env, jobject this, GSA2DomainController *domainController) {
    jstring domain = (*env)->GetObjectField(env, this, Gsa2DomainController.domain);
    domainController->domain = LJJNIEnvGetStringUTFChars(env, domain, NULL);

    jstring controller = (*env)->GetObjectField(env, this, Gsa2DomainController.controller);
    domainController->controller = LJJNIEnvGetStringUTFChars(env, controller, NULL);

    jstring key = (*env)->GetObjectField(env, this, Gsa2DomainController.key);
    domainController->key = LJJNIEnvGetStringUTFChars(env, key, NULL);

    jstring ip = (*env)->GetObjectField(env, this, Gsa2DomainController.ip);
    domainController->ip = LJJNIEnvGetStringUTFChars(env, ip, NULL);

    domainController->port = (*env)->GetIntField(env, this, Gsa2DomainController.port);

    jstring bssid = (*env)->GetObjectField(env, this, Gsa2DomainController.bssid);
    domainController->bssid = LJJNIEnvGetStringUTFChars(env, bssid, NULL);

    domainController->verified = (*env)->GetBooleanField(env, this, Gsa2DomainController.verified);
}

void Gsa2DomainControllerRelease(JNIEnv *env, jobject this, GSA2DomainController *domainController) {
    jstring domain = (*env)->GetObjectField(env, this, Gsa2DomainController.domain);
    LJJNIEnvReleaseStringUTFChars(env, domain, domainController->domain);

    jstring controller = (*env)->GetObjectField(env, this, Gsa2DomainController.controller);
    LJJNIEnvReleaseStringUTFChars(env, controller, domainController->controller);

    jstring key = (*env)->GetObjectField(env, this, Gsa2DomainController.key);
    LJJNIEnvReleaseStringUTFChars(env, key, domainController->key);

    jstring ip = (*env)->GetObjectField(env, this, Gsa2DomainController.ip);
    LJJNIEnvReleaseStringUTFChars(env, ip, domainController->ip);

    jstring bssid = (*env)->GetObjectField(env, this, Gsa2DomainController.bssid);
    LJJNIEnvReleaseStringUTFChars(env, bssid, domainController->bssid);
}

// Identity

Gsa2IdentityType Gsa2Identity = {0};

void Gsa2IdentityLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Identity");
    Gsa2Identity.class = (*env)->NewGlobalRef(env, class);

    Gsa2Identity.TYPE_PHONE = (*env)->GetStaticFieldID(env, class, "TYPE_PHONE", "Ljava/lang/String;");
    jstring TYPE_PHONE = (*env)->NewStringUTF(env, GSA2_IDENTITY_TYPE_PHONE);
    (*env)->SetStaticObjectField(env, class, Gsa2Identity.TYPE_PHONE, TYPE_PHONE);

    Gsa2Identity.TYPE_EMAIL = (*env)->GetStaticFieldID(env, class, "TYPE_EMAIL", "Ljava/lang/String;");
    jstring TYPE_EMAIL = (*env)->NewStringUTF(env, GSA2_IDENTITY_TYPE_EMAIL);
    (*env)->SetStaticObjectField(env, class, Gsa2Identity.TYPE_EMAIL, TYPE_EMAIL);

    Gsa2Identity.TYPE_MAC = (*env)->GetStaticFieldID(env, class, "TYPE_MAC", "Ljava/lang/String;");
    jstring TYPE_MAC = (*env)->NewStringUTF(env, GSA2_IDENTITY_TYPE_MAC);
    (*env)->SetStaticObjectField(env, class, Gsa2Identity.TYPE_MAC, TYPE_MAC);

    Gsa2Identity.type = (*env)->GetFieldID(env, class, "type", "Ljava/lang/String;");
    Gsa2Identity.value = (*env)->GetFieldID(env, class, "value", "Ljava/lang/String;");
    Gsa2Identity.account = (*env)->GetFieldID(env, class, "account", "Ljava/lang/String;");
    Gsa2Identity.controller = (*env)->GetFieldID(env, class, "controller", "Ljava/lang/String;");
    Gsa2Identity.init = (*env)->GetMethodID(env, class,"<init>", "()V");
}

void Gsa2IdentityUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Identity.class);
}

jobject Gsa2IdentityFrom(JNIEnv *env, GSA2Identity *identity) {
    jobject this = (*env)->NewObject(env, Gsa2Identity.class, Gsa2Identity.init);

    jstring type = (*env)->NewStringUTF(env, identity->type);
    (*env)->SetObjectField(env, this, Gsa2Identity.type, type);

    jstring value = (*env)->NewStringUTF(env, identity->value);
    (*env)->SetObjectField(env, this, Gsa2Identity.value, value);

    jstring account = (*env)->NewStringUTF(env, identity->account);
    (*env)->SetObjectField(env, this, Gsa2Identity.account, account);

    jstring controller = (*env)->NewStringUTF(env, identity->controller);
    (*env)->SetObjectField(env, this, Gsa2Identity.controller, controller);

    return this;
}

void Gsa2IdentityTo(JNIEnv *env, jobject this, GSA2Identity *identity) {
    jstring type = (*env)->GetObjectField(env, this, Gsa2Identity.type);
    identity->type = LJJNIEnvGetStringUTFChars(env, type, NULL);

    jstring value = (*env)->GetObjectField(env, this, Gsa2Identity.value);
    identity->value = LJJNIEnvGetStringUTFChars(env, value, NULL);

    jstring account = (*env)->GetObjectField(env, this, Gsa2Identity.account);
    identity->account = LJJNIEnvGetStringUTFChars(env, account, NULL);

    jstring controller = (*env)->GetObjectField(env, this, Gsa2Identity.controller);
    identity->controller = LJJNIEnvGetStringUTFChars(env, controller, NULL);
}

void Gsa2IdentityRelease(JNIEnv *env, jobject this, GSA2Identity *identity) {
    jstring type = (*env)->GetObjectField(env, this, Gsa2Identity.type);
    LJJNIEnvReleaseStringUTFChars(env, type, identity->type);

    jstring value = (*env)->GetObjectField(env, this, Gsa2Identity.value);
    LJJNIEnvReleaseStringUTFChars(env, value, identity->value);

    jstring account = (*env)->GetObjectField(env, this, Gsa2Identity.account);
    LJJNIEnvReleaseStringUTFChars(env, account, identity->account);

    jstring controller = (*env)->GetObjectField(env, this, Gsa2Identity.controller);
    LJJNIEnvReleaseStringUTFChars(env, controller, identity->controller);
}

void Gsa2IdentitiesTo(JNIEnv *env, jobjectArray this, GSA2SDKIdentities *sdkIdentities) {
    jsize n = LJJNIEnvGetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject identity = (*env)->GetObjectArrayElement(env, this, i);
        GSA2Identity *sdkIdentity = sdkIdentities->data[i];
        Gsa2IdentityTo(env, identity, sdkIdentity);
        (*env)->DeleteLocalRef(env, identity);
    }
}

void Gsa2IdentitiesRelease(JNIEnv *env, jobjectArray this, GSA2SDKIdentities *sdkIdentities) {
    jsize n = LJJNIEnvGetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject identity = (*env)->GetObjectArrayElement(env, this, i);
        GSA2Identity *sdkIdentity = sdkIdentities->data[i];
        Gsa2IdentityRelease(env, identity, sdkIdentity);
        (*env)->DeleteLocalRef(env, identity);
    }
}

// Invitation

Gsa2InvitationType Gsa2Invitation = {0};

void Gsa2InvitationLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Invitation");
    Gsa2Invitation.class = (*env)->NewGlobalRef(env, class);
    Gsa2Invitation.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    Gsa2Invitation.domain = (*env)->GetFieldID(env, class, "domain", "Ljava/lang/String;");
    Gsa2Invitation.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    Gsa2Invitation.expiration = (*env)->GetFieldID(env, class, "expiration", "Ljava/time/Instant;");
    Gsa2Invitation.access = (*env)->GetFieldID(env, class, "access", "J");
    Gsa2Invitation.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2InvitationUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Invitation.class);
}

jobject Gsa2InvitationFrom(JNIEnv *env, GSA2Invitation *invitation) {
    jobject this = (*env)->NewObject(env, Gsa2Invitation.class, Gsa2Invitation.init);

    jstring id = (*env)->NewStringUTF(env, invitation->id);
    (*env)->SetObjectField(env, this, Gsa2Invitation.id, id);

    jstring domain = (*env)->NewStringUTF(env, invitation->domain);
    (*env)->SetObjectField(env, this, Gsa2Invitation.domain, domain);

    jstring name = (*env)->NewStringUTF(env, invitation->name);
    (*env)->SetObjectField(env, this, Gsa2Invitation.name, name);

    jstring stringExpiration = (*env)->NewStringUTF(env, invitation->expiration);
    jobject expiration = (*env)->CallStaticObjectMethod(env, JTInstant.class, JTInstant.parse, stringExpiration);
    (*env)->ExceptionClear(env);
    (*env)->SetObjectField(env, this, Gsa2Invitation.expiration, expiration);

    (*env)->SetLongField(env, this, Gsa2Invitation.access, invitation->access);

    return this;
}

void Gsa2InvitationTo(JNIEnv *env, jobject this, GSA2Invitation *invitation) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Invitation.id);
    invitation->id = LJJNIEnvGetStringUTFChars(env, id, NULL);

    jstring domain = (*env)->GetObjectField(env, this, Gsa2Invitation.domain);
    invitation->domain = LJJNIEnvGetStringUTFChars(env, domain, NULL);

    jstring name = (*env)->GetObjectField(env, this, Gsa2Invitation.name);
    invitation->name = LJJNIEnvGetStringUTFChars(env, name, NULL);

    jobject expiration = (*env)->GetObjectField(env, this, Gsa2Invitation.expiration);
    jlong longExpiration = (*env)->CallLongMethod(env, expiration, JTInstant.getEpochSecond);
    (void)asprintf(&invitation->expiration, "%li", longExpiration);

    invitation->access = (*env)->GetLongField(env, this, Gsa2Invitation.access);
}

void Gsa2InvitationRelease(JNIEnv *env, jobject this, GSA2Invitation *invitation) {
    jstring id = (*env)->GetObjectField(env, this, Gsa2Invitation.id);
    LJJNIEnvReleaseStringUTFChars(env, id, invitation->id);

    jstring domain = (*env)->GetObjectField(env, this, Gsa2Invitation.domain);
    LJJNIEnvReleaseStringUTFChars(env, domain, invitation->domain);

    jstring name = (*env)->GetObjectField(env, this, Gsa2Invitation.name);
    LJJNIEnvReleaseStringUTFChars(env, name, invitation->name);

    free(invitation->expiration);
}

jobjectArray Gsa2InvitationsFrom(JNIEnv *env, GSA2SDKInvitations *sdkInvitations) {
    jobjectArray this = (*env)->NewObjectArray(env, sdkInvitations->n, Gsa2Invitation.class, NULL);

    for (int i = 0; i < sdkInvitations->n; i++) {
        GSA2Invitation *sdkInvitation = sdkInvitations->data[i];
        jobject invitation = Gsa2InvitationFrom(env, sdkInvitation);
        (*env)->SetObjectArrayElement(env, this, i, invitation);
        (*env)->DeleteLocalRef(env, invitation);
    }

    return this;
}

// Token

Gsa2TokenType Gsa2Token = {0};

void Gsa2TokenLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Token");
    Gsa2Token.class = (*env)->NewGlobalRef(env, class);

    Gsa2Token.SUBJECT_ACCOUNT = (*env)->GetStaticFieldID(env, class, "SUBJECT_ACCOUNT", "Ljava/lang/String;");
    jstring SUBJECT_ACCOUNT = (*env)->NewStringUTF(env, GSA2_TOKEN_SUBJECT_ACCOUNT);
    (*env)->SetStaticObjectField(env, class, Gsa2Token.SUBJECT_ACCOUNT, SUBJECT_ACCOUNT);

    Gsa2Token.SUBJECT_CONTROLLER = (*env)->GetStaticFieldID(env, class, "SUBJECT_CONTROLLER", "Ljava/lang/String;");
    jstring SUBJECT_CONTROLLER = (*env)->NewStringUTF(env, GSA2_TOKEN_SUBJECT_CONTROLLER);
    (*env)->SetStaticObjectField(env, class, Gsa2Token.SUBJECT_CONTROLLER, SUBJECT_CONTROLLER);

    Gsa2Token.subject = (*env)->GetFieldID(env, class, "subject", "Ljava/lang/String;");
    Gsa2Token.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    Gsa2Token.access = (*env)->GetFieldID(env, class, "access", "Ljava/lang/String;");
    Gsa2Token.refresh = (*env)->GetFieldID(env, class, "refresh", "Ljava/lang/String;");
    Gsa2Token.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2TokenUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Token.class);
}

jobject Gsa2TokenFrom(JNIEnv *env, GSA2Token *token) {
    jobject this = (*env)->NewObject(env, Gsa2Token.class, Gsa2Token.init);

    jstring subject = (*env)->NewStringUTF(env, token->subject);
    (*env)->SetObjectField(env, this, Gsa2Token.subject, subject);

    jstring id = (*env)->NewStringUTF(env, token->id);
    (*env)->SetObjectField(env, this, Gsa2Token.id, id);

    jstring access = (*env)->NewStringUTF(env, token->access);
    (*env)->SetObjectField(env, this, Gsa2Token.access, access);

    jstring refresh = (*env)->NewStringUTF(env, token->refresh);
    (*env)->SetObjectField(env, this, Gsa2Token.refresh, refresh);

    return this;
}

jobjectArray Gsa2TokensFrom(JNIEnv *env, GSA2SDKTokens *sdkTokens) {
    jobjectArray this = (*env)->NewObjectArray(env, sdkTokens->n, Gsa2Token.class, NULL);

    for (int i = 0; i < sdkTokens->n; i++) {
        GSA2Token *sdkToken = sdkTokens->data[i];
        jobject token = Gsa2TokenFrom(env, sdkToken);
        (*env)->SetObjectArrayElement(env, this, i, token);
        (*env)->DeleteLocalRef(env, token);
    }

    return this;
}

// Credential

Gsa2CredentialType Gsa2Credential = {0};

void Gsa2CredentialLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2Credential");
    Gsa2Credential.class = (*env)->NewGlobalRef(env, class);

    Gsa2Credential.TYPE_PASSWORD = (*env)->GetStaticFieldID(env, class, "TYPE_PASSWORD", "Ljava/lang/String;");
    jstring TYPE_PASSWORD = (*env)->NewStringUTF(env, GSA2_CREDENTIAL_TYPE_PASSWORD);
    (*env)->SetStaticObjectField(env, class, Gsa2Credential.TYPE_PASSWORD, TYPE_PASSWORD);

    Gsa2Credential.TYPE_CODE = (*env)->GetStaticFieldID(env, class, "TYPE_CODE", "Ljava/lang/String;");
    jstring TYPE_CODE = (*env)->NewStringUTF(env, GSA2_CREDENTIAL_TYPE_CODE);
    (*env)->SetStaticObjectField(env, class, Gsa2Credential.TYPE_CODE, TYPE_CODE);

    Gsa2Credential.TYPE_SERIAL = (*env)->GetStaticFieldID(env, class, "TYPE_SERIAL", "Ljava/lang/String;");
    jstring TYPE_SERIAL = (*env)->NewStringUTF(env, GSA2_CREDENTIAL_TYPE_SERIAL);
    (*env)->SetStaticObjectField(env, class, Gsa2Credential.TYPE_SERIAL, TYPE_SERIAL);

    Gsa2Credential.type = (*env)->GetFieldID(env, class, "type", "Ljava/lang/String;");
    Gsa2Credential.value = (*env)->GetFieldID(env, class, "value", "Ljava/lang/String;");
    Gsa2Credential.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2CredentialUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2Credential.class);
}

void Gsa2CredentialTo(JNIEnv *env, jobject this, GSA2Credential *credential) {
    jstring type = (*env)->GetObjectField(env, this, Gsa2Credential.type);
    credential->type = LJJNIEnvGetStringUTFChars(env, type, NULL);

    jstring value = (*env)->GetObjectField(env, this, Gsa2Credential.value);
    credential->value = LJJNIEnvGetStringUTFChars(env, value, NULL);
}

void Gsa2CredentialRelease(JNIEnv *env, jobject this, GSA2Credential *credential) {
    jstring type = (*env)->GetObjectField(env, this, Gsa2Credential.type);
    LJJNIEnvReleaseStringUTFChars(env, type, credential->type);

    jstring value = (*env)->GetObjectField(env, this, Gsa2Credential.value);
    LJJNIEnvReleaseStringUTFChars(env, value, credential->value);
}

// AccountFull

Gsa2AccountFullType Gsa2AccountFull = {0};

void Gsa2AccountFullLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2AccountFull");
    Gsa2AccountFull.class = (*env)->NewGlobalRef(env, class);
    Gsa2AccountFull.account = (*env)->GetFieldID(env, class, "account", "Llibrary/gsauth2/Gsa2Account;");
    Gsa2AccountFull.accountDomain = (*env)->GetFieldID(env, class, "accountDomain", "Llibrary/gsauth2/Gsa2AccountDomain;");
    Gsa2AccountFull.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2AccountFullUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2AccountFull.class);
}

jobject Gsa2AccountFullFrom(JNIEnv *env, GSA2AccountFull *accountFull) {
    jobject this = (*env)->NewObject(env, Gsa2AccountFull.class, Gsa2AccountFull.init);

    jobject account = Gsa2AccountFrom(env, accountFull->account);
    (*env)->SetObjectField(env, this, Gsa2AccountFull.account, account);

    jobject accountDomain = Gsa2AccountDomainFrom(env, accountFull->account_domain);
    (*env)->SetObjectField(env, this, Gsa2AccountFull.accountDomain, accountDomain);

    return this;
}

jobjectArray Gsa2AccountsFullFrom(JNIEnv *env, GSA2SDKAccountsFull *sdkAccountsFull) {
    jobjectArray this = (*env)->NewObjectArray(env, sdkAccountsFull->n, Gsa2AccountFull.class, NULL);

    for (int i = 0; i < sdkAccountsFull->n; i++) {
        GSA2AccountFull *sdkAccountFull = sdkAccountsFull->data[i];
        jobject accountFull = Gsa2AccountFullFrom(env, sdkAccountFull);
        (*env)->SetObjectArrayElement(env, this, i, accountFull);
        (*env)->DeleteLocalRef(env, accountFull);
    }

    return this;
}

// DomainFull

Gsa2DomainFullType Gsa2DomainFull = {0};

void Gsa2DomainFullLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2DomainFull");
    Gsa2DomainFull.class = (*env)->NewGlobalRef(env, class);
    Gsa2DomainFull.domain = (*env)->GetFieldID(env, class, "domain", "Llibrary/gsauth2/Gsa2Domain;");
    Gsa2DomainFull.accountDomain = (*env)->GetFieldID(env, class, "accountDomain", "Llibrary/gsauth2/Gsa2AccountDomain;");
    Gsa2DomainFull.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2DomainFullUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2DomainFull.class);
}

jobject Gsa2DomainFullFrom(JNIEnv *env, GSA2DomainFull *domainFull) {
    jobject this = (*env)->NewObject(env, Gsa2DomainFull.class, Gsa2DomainFull.init);

    jobject domain = Gsa2DomainFrom(env, domainFull->domain);
    (*env)->SetObjectField(env, this, Gsa2DomainFull.domain, domain);

    jobject accountDomain = Gsa2AccountDomainFrom(env, domainFull->account_domain);
    (*env)->SetObjectField(env, this, Gsa2DomainFull.accountDomain, accountDomain);

    return this;
}

jobjectArray Gsa2DomainsFullFrom(JNIEnv *env, GSA2SDKDomainsFull *sdkDomainsFull) {
    jobjectArray this = (*env)->NewObjectArray(env, sdkDomainsFull->n, Gsa2DomainFull.class, NULL);

    for (int i = 0; i < sdkDomainsFull->n; i++) {
        GSA2DomainFull *sdkDomainFull = sdkDomainsFull->data[i];
        jobject domainFull = Gsa2DomainFullFrom(env, sdkDomainFull);
        (*env)->SetObjectArrayElement(env, this, i, domainFull);
        (*env)->DeleteLocalRef(env, domainFull);
    }

    return this;
}

// ControllerFull

Gsa2ControllerFullType Gsa2ControllerFull = {0};

void Gsa2ControllerFullLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsauth2/Gsa2ControllerFull");
    Gsa2ControllerFull.class = (*env)->NewGlobalRef(env, class);
    Gsa2ControllerFull.controller = (*env)->GetFieldID(env, class, "controller", "Llibrary/gsauth2/Gsa2Controller;");
    Gsa2ControllerFull.domainController = (*env)->GetFieldID(env, class, "domainController", "Llibrary/gsauth2/Gsa2DomainController;");
    Gsa2ControllerFull.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void Gsa2ControllerFullUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, Gsa2ControllerFull.class);
}

jobject Gsa2ControllerFullFrom(JNIEnv *env, GSA2ControllerFull *controllerFull) {
    jobject this = (*env)->NewObject(env, Gsa2ControllerFull.class, Gsa2ControllerFull.init);

    jobject controller = Gsa2ControllerFrom(env, controllerFull->controller);
    (*env)->SetObjectField(env, this, Gsa2ControllerFull.controller, controller);

    jobject domainController = Gsa2DomainControllerFrom(env, controllerFull->domain_controller);
    (*env)->SetObjectField(env, this, Gsa2ControllerFull.domainController, domainController);

    return this;
}

void Gsa2ControllerFullTo(JNIEnv *env, jobject this, GSA2ControllerFull *controllerFull) {
    jobject controller = (*env)->GetObjectField(env, this, Gsa2ControllerFull.controller);
    Gsa2ControllerTo(env, controller, controllerFull->controller);

    jobject domainController = (*env)->GetObjectField(env, this, Gsa2ControllerFull.domainController);
    Gsa2DomainControllerTo(env, domainController, controllerFull->domain_controller);
}

void Gsa2ControllerFullRelease(JNIEnv *env, jobject this, GSA2ControllerFull *controllerFull) {
    jobject controller = (*env)->GetObjectField(env, this, Gsa2ControllerFull.controller);
    Gsa2ControllerRelease(env, controller, controllerFull->controller);

    jobject domainController = (*env)->GetObjectField(env, this, Gsa2ControllerFull.domainController);
    Gsa2DomainControllerRelease(env, domainController, controllerFull->domain_controller);
}

jobjectArray Gsa2ControllersFullFrom(JNIEnv *env, GSA2SDKControllersFull *sdkControllersFull) {
    jobjectArray this = (*env)->NewObjectArray(env, sdkControllersFull->n, Gsa2ControllerFull.class, NULL);

    for (int i = 0; i < sdkControllersFull->n; i++) {
        GSA2ControllerFull *sdkControllerFull = sdkControllersFull->data[i];
        jobject controllerFull = Gsa2ControllerFullFrom(env, sdkControllerFull);
        (*env)->SetObjectArrayElement(env, this, i, controllerFull);
        (*env)->DeleteLocalRef(env, controllerFull);
    }

    return this;
}

void Gsa2ControllersFullTo(JNIEnv *env, jobjectArray this, GSA2SDKControllersFull *sdkControllersFull) {
    jsize n = LJJNIEnvGetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject controllerFull = (*env)->GetObjectArrayElement(env, this, i);
        GSA2ControllerFull *sdkControllerFull = sdkControllersFull->data[i];
        Gsa2ControllerFullTo(env, controllerFull, sdkControllerFull);
        (*env)->DeleteLocalRef(env, controllerFull);
    }
}

void Gsa2ControllersFullRelease(JNIEnv *env, jobjectArray this, GSA2SDKControllersFull *sdkControllersFull) {
    jsize n = LJJNIEnvGetArrayLength(env, this);

    for (jsize i = 0; i < n; i++) {
        jobject controllerFull = (*env)->GetObjectArrayElement(env, this, i);
        GSA2ControllerFull *sdkControllerFull = sdkControllersFull->data[i];
        Gsa2ControllerFullRelease(env, controllerFull, sdkControllerFull);
        (*env)->DeleteLocalRef(env, controllerFull);
    }
}
