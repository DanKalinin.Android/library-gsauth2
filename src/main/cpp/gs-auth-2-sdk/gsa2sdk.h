//
// Created by root on 01.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SDK_H
#define LIBRARY_GS_AUTH_2_GSA2SDK_H

#include "gsa2schemacommon.h"

// Error

typedef struct {
    char *domain;
    int code;
    char *message;
} GSA2SDKError;

void gsa2_sdk_error_free(GSA2SDKError *self);

// Account

void gsa2_sdk_account_free(GSA2Account *self);

typedef struct {
    GSA2Account **data;
    int n;
} GSA2SDKAccounts;

void gsa2_sdk_accounts_free(GSA2SDKAccounts *self);

// Domain

void gsa2_sdk_domain_free(GSA2Domain *self);

typedef struct {
    GSA2Domain **data;
    int n;
} GSA2SDKDomains;

void gsa2_sdk_domains_free(GSA2SDKDomains *self);

// Controller

void gsa2_sdk_controller_free(GSA2Controller *self);

typedef struct {
    GSA2Controller **data;
    int n;
} GSA2SDKControllers;

void gsa2_sdk_controllers_free(GSA2SDKControllers *self);

// AccountDomain

void gsa2_sdk_account_domain_free(GSA2AccountDomain *self);

typedef struct {
    GSA2AccountDomain **data;
    int n;
} GSA2SDKAccountDomains;

void gsa2_sdk_account_domains_free(GSA2SDKAccountDomains *self);

// DomainController

void gsa2_sdk_domain_controller_free(GSA2DomainController *self);

typedef struct {
    GSA2DomainController **data;
    int n;
} GSA2SDKDomainControllers;

void gsa2_sdk_domain_controllers_free(GSA2SDKDomainControllers *self);

// Identity

void gsa2_sdk_identity_free(GSA2Identity *self);

typedef struct {
    GSA2Identity **data;
    int n;
} GSA2SDKIdentities;

void gsa2_sdk_identities_free(GSA2SDKIdentities *self);

// Invitation

void gsa2_sdk_invitation_free(GSA2Invitation *self);

typedef struct {
    GSA2Invitation **data;
    int n;
} GSA2SDKInvitations;

void gsa2_sdk_invitations_free(GSA2SDKInvitations *self);

// Token

void gsa2_sdk_token_free(GSA2Token *self);

typedef struct {
    GSA2Token **data;
    int n;
} GSA2SDKTokens;

void gsa2_sdk_tokens_free(GSA2SDKTokens *self);

// AccountFull

void gsa2_sdk_account_full_free(GSA2AccountFull *self);

typedef struct {
    GSA2AccountFull **data;
    int n;
} GSA2SDKAccountsFull;

void gsa2_sdk_accounts_full_free(GSA2SDKAccountsFull *self);

// DomainFull

void gsa2_sdk_domain_full_free(GSA2DomainFull *self);

typedef struct {
    GSA2DomainFull **data;
    int n;
} GSA2SDKDomainsFull;

void gsa2_sdk_domains_full_free(GSA2SDKDomainsFull *self);

// ControllerFull

void gsa2_sdk_controller_full_free(GSA2ControllerFull *self);

typedef struct {
    GSA2ControllerFull **data;
    int n;
} GSA2SDKControllersFull;

void gsa2_sdk_controllers_full_free(GSA2SDKControllersFull *self);

// Database

typedef struct _GSA2SDKDatabase GSA2SDKDatabase;

GSA2SDKDatabase *gsa2_sdk_database_new(char *file, GSA2SDKError **sdk_error);
void gsa2_sdk_database_free(GSA2SDKDatabase *self);

// Account

GSA2SDKAccounts *gsa2_sdk_database_account_select_by_id(GSA2SDKDatabase *self, char *id, GSA2SDKError **sdk_error);
int gsa2_sdk_database_account_delete_by_id(GSA2SDKDatabase *self, char *id, GSA2SDKError **sdk_error);

// Invitation

GSA2SDKInvitations *gsa2_sdk_database_invitation_select_by_id(GSA2SDKDatabase *self, char *id, GSA2SDKError **sdk_error);
GSA2SDKInvitations *gsa2_sdk_database_invitation_select_by_domain(GSA2SDKDatabase *self, char *tail, char *domain, GSA2SDKError **sdk_error);

// Token

GSA2SDKTokens *gsa2_sdk_database_token_select_by_subject_and_id(GSA2SDKDatabase *self, char *subject, char *id, GSA2SDKError **sdk_error);

// AccountFull

GSA2SDKAccountsFull *gsa2_sdk_database_guest_full_select_by_domain(GSA2SDKDatabase *self, char *tail, char *domain, GSA2SDKError **sdk_error);
GSA2SDKAccountsFull *gsa2_sdk_database_guest_full_select_by_domain_and_guest(GSA2SDKDatabase *self, char *domain, char *guest, GSA2SDKError **sdk_error);

// DomainFull

GSA2SDKDomainsFull *gsa2_sdk_database_domain_full_select_by_account(GSA2SDKDatabase *self, char *tail, char *account, GSA2SDKError **sdk_error);
GSA2SDKDomainsFull *gsa2_sdk_database_domain_full_select_by_account_and_domain(GSA2SDKDatabase *self, char *account, char *domain, GSA2SDKError **sdk_error);

// ControllerFull

GSA2SDKControllersFull *gsa2_sdk_database_controller_full_select_by_domain(GSA2SDKDatabase *self, char *tail, char *domain, GSA2SDKError **sdk_error);
GSA2SDKControllersFull *gsa2_sdk_database_controller_full_select_by_domain_and_controller(GSA2SDKDatabase *self, char *domain, char *controller, GSA2SDKError **sdk_error);

// Client

typedef struct _GSA2SDKClient GSA2SDKClient;

GSA2SDKClient *gsa2_sdk_client_new(char *sdk_base, GSA2SDKDatabase *sdk_database);
void gsa2_sdk_client_free(GSA2SDKClient *self);

// IdentityCheck

typedef void (*GSA2SDKClientIdentityCheckCallback)(int available, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_identity_check(GSA2SDKClient *self, GSA2Identity *identity, GSA2SDKClientIdentityCheckCallback sdk_callback, void *data);

// CodeSend

typedef void (*GSA2SDKClientCodeSendCallback)(long sdk_expiration, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_code_send(GSA2SDKClient *self, GSA2Identity *identity, GSA2SDKClientCodeSendCallback sdk_callback, void *data);

// AccountCreate

typedef void (*GSA2SDKClientAccountCreateCallback)(GSA2Account *account, long sdk_expiration, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_account_create(GSA2SDKClient *self, GSA2Account *account, GSA2SDKIdentities *sdk_identities, GSA2SDKClientAccountCreateCallback sdk_callback, void *data);

// AccountGet

typedef void (*GSA2SDKClientAccountGetCallback)(GSA2Account *account, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_account_get(GSA2SDKClient *self, char *access, GSA2SDKClientAccountGetCallback sdk_callback, void *data);

// PasswordUpdate

typedef void (*GSA2SDKClientPasswordUpdateCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_password_update(GSA2SDKClient *self, char *access, char *password, GSA2SDKClientPasswordUpdateCallback sdk_callback, void *data);

// DomainCreate

typedef void (*GSA2SDKClientDomainCreateCallback)(GSA2DomainFull *domain_full, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_domain_create(GSA2SDKClient *self, char *access, GSA2Domain *domain, GSA2SDKControllersFull *sdk_controllers_full, GSA2SDKClientDomainCreateCallback sdk_callback, void *data);

// DomainRename

typedef void (*GSA2SDKClientDomainRenameCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_domain_rename(GSA2SDKClient *self, char *access, char *domain_id, char *name, GSA2SDKClientDomainRenameCallback sdk_callback, void *data);

// DomainDelete

typedef void (*GSA2SDKClientDomainDeleteCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_domain_delete(GSA2SDKClient *self, char *access, char *domain_id, GSA2SDKClientDomainDeleteCallback sdk_callback, void *data);

// DomainExit

typedef void (*GSA2SDKClientDomainExitCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_domain_exit(GSA2SDKClient *self, char *access, char *domain_id, GSA2SDKClientDomainExitCallback sdk_callback, void *data);

// GuestDelete

typedef void (*GSA2SDKClientGuestDeleteCallback)(GSA2DomainFull *domain_full, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_guest_delete(GSA2SDKClient *self, char *access, char *domain_id, char *guest_id, GSA2SDKClientGuestDeleteCallback sdk_callback, void *data);

// InvitationCreate

typedef void (*GSA2SDKClientInvitationCreateCallback)(GSA2Invitation *invitation, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_invitation_create(GSA2SDKClient *self, char *access, char *domain_id, GSA2Invitation *invitation, GSA2SDKClientInvitationCreateCallback sdk_callback, void *data);

// InvitationRevoke

typedef void (*GSA2SDKClientInvitationRevokeCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_invitation_revoke(GSA2SDKClient *self, char *access, char *domain_id, char *invitation_id, GSA2SDKClientInvitationRevokeCallback sdk_callback, void *data);

// InvitationAccept

typedef void (*GSA2SDKClientInvitationAcceptCallback)(GSA2DomainFull *domain_full, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_invitation_accept(GSA2SDKClient *self, char *access, char *invitation_id, GSA2SDKClientInvitationAcceptCallback sdk_callback, void *data);

// CodeGet

typedef void (*GSA2SDKClientCodeGetCallback)(char *code, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_code_get(GSA2SDKClient *self, char *access, char *domain_id, GSA2SDKClientCodeGetCallback sdk_callback, void *data);

// ControllersAdd

typedef void (*GSA2SDKClientControllersAddCallback)(GSA2SDKControllersFull *sdk_controllers_full, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_controllers_add(GSA2SDKClient *self, char *access, char *domain_id, GSA2SDKControllersFull *sdk_controllers_full, GSA2SDKClientControllersAddCallback sdk_callback, void *data);

// ControllerRemove

typedef void (*GSA2SDKClientControllerRemoveCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_controller_remove(GSA2SDKClient *self, char *access, char *domain_id, char *controller_id, GSA2SDKClientControllerRemoveCallback sdk_callback, void *data);

// TokenIssue

typedef void (*GSA2SDKClientTokenIssueCallback)(GSA2Token *token, GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_token_issue(GSA2SDKClient *self, GSA2Identity *identity, GSA2Credential *credential, GSA2SDKClientTokenIssueCallback sdk_callback, void *data);

// TokenRevoke

typedef void (*GSA2SDKClientTokenRevokeCallback)(GSA2SDKError *sdk_error, void *data);
void gsa2_sdk_client_token_revoke(GSA2SDKClient *self, char *access, GSA2SDKClientTokenRevokeCallback sdk_callback, void *data);

// Main

void gsa2_sdk_init(void);

#endif //LIBRARY_GS_AUTH_2_GSA2SDK_H
