//
// Created by root on 27.05.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SCHEMACOMMON_H
#define LIBRARY_GS_AUTH_2_GSA2SCHEMACOMMON_H

typedef struct {
    char *id;
    char *name;
    char *password;
} GSA2Account;

typedef struct {
    char *id;
    char *owner;
    char *name;
} GSA2Domain;

typedef struct {
    char *id;
    char *mac;
    char *model;
    char *serial;
} GSA2Controller;

typedef struct {
    char *account;
    char *domain;
    char *expiration;
    int current;
} GSA2AccountDomain;

typedef struct {
    char *domain;
    char *controller;
    char *key;
    char *ip;
    int port;
    char *bssid;
    int verified;
} GSA2DomainController;

#define GSA2_IDENTITY_TYPE_PHONE "phone"
#define GSA2_IDENTITY_TYPE_EMAIL "email"
#define GSA2_IDENTITY_TYPE_MAC "macAddress"

typedef struct {
    char *type;
    char *value;
    char *account;
    char *controller;
} GSA2Identity;

typedef struct {
    char *id;
    char *domain;
    char *name;
    char *expiration;
    long access;
} GSA2Invitation;

#define GSA2_TOKEN_SUBJECT_ACCOUNT "account"
#define GSA2_TOKEN_SUBJECT_CONTROLLER "controller"

typedef struct {
    char *subject;
    char *id;
    char *access;
    char *refresh;
} GSA2Token;

#define GSA2_CREDENTIAL_TYPE_PASSWORD "password"
#define GSA2_CREDENTIAL_TYPE_CODE "activationCode"
#define GSA2_CREDENTIAL_TYPE_SERIAL "serialNo"

typedef struct {
    char *type;
    char *value;
} GSA2Credential;

typedef struct {
    GSA2Account *account;
    GSA2AccountDomain *account_domain;
} GSA2AccountFull;

typedef struct {
    GSA2Domain *domain;
    GSA2AccountDomain *account_domain;
} GSA2DomainFull;

typedef struct {
    GSA2Controller *controller;
    GSA2DomainController *domain_controller;
} GSA2ControllerFull;

#endif //LIBRARY_GS_AUTH_2_GSA2SCHEMACOMMON_H
