//
// Created by Dan Kalinin on 9/5/20.
//

#ifndef LIBRARY_GSAUTH2_GSA2SCHEMA_H
#define LIBRARY_GSAUTH2_GSA2SCHEMA_H

#include "Gsa2Main.h"

// Account

typedef struct {
    jclass class;
    jfieldID id;
    jfieldID name;
    jfieldID password;
    jmethodID init;
} Gsa2AccountType;

extern Gsa2AccountType Gsa2Account;

void Gsa2AccountLoad(JNIEnv *env);
void Gsa2AccountUnload(JNIEnv *env);
jobject Gsa2AccountFrom(JNIEnv *env, GSA2Account *account);
void Gsa2AccountTo(JNIEnv *env, jobject this, GSA2Account *account);
void Gsa2AccountRelease(JNIEnv *env, jobject this, GSA2Account *account);

jobjectArray Gsa2AccountsFrom(JNIEnv *env, GSA2SDKAccounts *sdkAccounts);

// Domain

typedef struct {
    jclass class;
    jfieldID id;
    jfieldID owner;
    jfieldID name;
    jmethodID init;
} Gsa2DomainType;

extern Gsa2DomainType Gsa2Domain;

void Gsa2DomainLoad(JNIEnv *env);
void Gsa2DomainUnload(JNIEnv *env);
jobject Gsa2DomainFrom(JNIEnv *env, GSA2Domain *domain);
void Gsa2DomainTo(JNIEnv *env, jobject this, GSA2Domain *domain);
void Gsa2DomainRelease(JNIEnv *env, jobject this, GSA2Domain *domain);

// Controller

typedef struct {
    jclass class;
    jfieldID id;
    jfieldID mac;
    jfieldID model;
    jfieldID serial;
    jmethodID init;
} Gsa2ControllerType;

extern Gsa2ControllerType Gsa2Controller;

void Gsa2ControllerLoad(JNIEnv *env);
void Gsa2ControllerUnload(JNIEnv *env);
jobject Gsa2ControllerFrom(JNIEnv *env, GSA2Controller *controller);
void Gsa2ControllerTo(JNIEnv *env, jobject this, GSA2Controller *controller);
void Gsa2ControllerRelease(JNIEnv *env, jobject this, GSA2Controller *controller);

// AccountDomain

typedef struct {
    jclass class;
    jfieldID account;
    jfieldID domain;
    jfieldID expiration;
    jfieldID current;
    jmethodID init;
} Gsa2AccountDomainType;

extern Gsa2AccountDomainType Gsa2AccountDomain;

void Gsa2AccountDomainLoad(JNIEnv *env);
void Gsa2AccountDomainUnload(JNIEnv *env);
jobject Gsa2AccountDomainFrom(JNIEnv *env, GSA2AccountDomain *accountDomain);

// DomainController

typedef struct {
    jclass class;
    jfieldID domain;
    jfieldID controller;
    jfieldID key;
    jfieldID ip;
    jfieldID port;
    jfieldID bssid;
    jfieldID verified;
    jmethodID init;
} Gsa2DomainControllerType;

extern Gsa2DomainControllerType Gsa2DomainController;

void Gsa2DomainControllerLoad(JNIEnv *env);
void Gsa2DomainControllerUnload(JNIEnv *env);
jobject Gsa2DomainControllerFrom(JNIEnv *env, GSA2DomainController *domainController);
void Gsa2DomainControllerTo(JNIEnv *env, jobject this, GSA2DomainController *domainController);
void Gsa2DomainControllerRelease(JNIEnv *env, jobject this, GSA2DomainController *domainController);

// Identity

typedef struct {
    jclass class;
    jfieldID TYPE_PHONE;
    jfieldID TYPE_EMAIL;
    jfieldID TYPE_MAC;
    jfieldID type;
    jfieldID value;
    jfieldID account;
    jfieldID controller;
    jmethodID init;
} Gsa2IdentityType;

extern Gsa2IdentityType Gsa2Identity;

void Gsa2IdentityLoad(JNIEnv *env);
void Gsa2IdentityUnload(JNIEnv *env);
jobject Gsa2IdentityFrom(JNIEnv *env, GSA2Identity *identity);
void Gsa2IdentityTo(JNIEnv *env, jobject this, GSA2Identity *identity);
void Gsa2IdentityRelease(JNIEnv *env, jobject this, GSA2Identity *identity);

void Gsa2IdentitiesTo(JNIEnv *env, jobjectArray this, GSA2SDKIdentities *sdkIdentities);
void Gsa2IdentitiesRelease(JNIEnv *env, jobjectArray this, GSA2SDKIdentities *sdkIdentities);

// Invitation

typedef struct {
    jclass class;
    jfieldID id;
    jfieldID domain;
    jfieldID name;
    jfieldID expiration;
    jfieldID access;
    jmethodID init;
} Gsa2InvitationType;

extern Gsa2InvitationType Gsa2Invitation;

void Gsa2InvitationLoad(JNIEnv *env);
void Gsa2InvitationUnload(JNIEnv *env);
jobject Gsa2InvitationFrom(JNIEnv *env, GSA2Invitation *invitation);
void Gsa2InvitationTo(JNIEnv *env, jobject this, GSA2Invitation *invitation);
void Gsa2InvitationRelease(JNIEnv *env, jobject this, GSA2Invitation *invitation);

jobjectArray Gsa2InvitationsFrom(JNIEnv *env, GSA2SDKInvitations *sdkInvitations);

// Token

typedef struct {
    jclass class;
    jfieldID SUBJECT_ACCOUNT;
    jfieldID SUBJECT_CONTROLLER;
    jfieldID subject;
    jfieldID id;
    jfieldID access;
    jfieldID refresh;
    jmethodID init;
} Gsa2TokenType;

extern Gsa2TokenType Gsa2Token;

void Gsa2TokenLoad(JNIEnv *env);
void Gsa2TokenUnload(JNIEnv *env);
jobject Gsa2TokenFrom(JNIEnv *env, GSA2Token *token);

jobjectArray Gsa2TokensFrom(JNIEnv *env, GSA2SDKTokens *sdkTokens);

// Credential

typedef struct {
    jclass class;
    jfieldID TYPE_PASSWORD;
    jfieldID TYPE_CODE;
    jfieldID TYPE_SERIAL;
    jfieldID type;
    jfieldID value;
    jmethodID init;
} Gsa2CredentialType;

extern Gsa2CredentialType Gsa2Credential;

void Gsa2CredentialLoad(JNIEnv *env);
void Gsa2CredentialUnload(JNIEnv *env);
void Gsa2CredentialTo(JNIEnv *env, jobject this, GSA2Credential *credential);
void Gsa2CredentialRelease(JNIEnv *env, jobject this, GSA2Credential *credential);

// AccountFull

typedef struct {
    jclass class;
    jfieldID account;
    jfieldID accountDomain;
    jmethodID init;
} Gsa2AccountFullType;

extern Gsa2AccountFullType Gsa2AccountFull;

void Gsa2AccountFullLoad(JNIEnv *env);
void Gsa2AccountFullUnload(JNIEnv *env);
jobject Gsa2AccountFullFrom(JNIEnv *env, GSA2AccountFull *accountFull);

jobjectArray Gsa2AccountsFullFrom(JNIEnv *env, GSA2SDKAccountsFull *sdkAccountsFull);

// DomainFull

typedef struct {
    jclass class;
    jfieldID domain;
    jfieldID accountDomain;
    jmethodID init;
} Gsa2DomainFullType;

extern Gsa2DomainFullType Gsa2DomainFull;

void Gsa2DomainFullLoad(JNIEnv *env);
void Gsa2DomainFullUnload(JNIEnv *env);
jobject Gsa2DomainFullFrom(JNIEnv *env, GSA2DomainFull *domainFull);

jobjectArray Gsa2DomainsFullFrom(JNIEnv *env, GSA2SDKDomainsFull *sdkDomainsFull);

// ControllerFull

typedef struct {
    jclass class;
    jfieldID controller;
    jfieldID domainController;
    jmethodID init;
} Gsa2ControllerFullType;

extern Gsa2ControllerFullType Gsa2ControllerFull;

void Gsa2ControllerFullLoad(JNIEnv *env);
void Gsa2ControllerFullUnload(JNIEnv *env);
jobject Gsa2ControllerFullFrom(JNIEnv *env, GSA2ControllerFull *controllerFull);
void Gsa2ControllerFullTo(JNIEnv *env, jobject this, GSA2ControllerFull *controllerFull);
void Gsa2ControllerFullRelease(JNIEnv *env, jobject this, GSA2ControllerFull *controllerFull);

jobjectArray Gsa2ControllersFullFrom(JNIEnv *env, GSA2SDKControllersFull *sdkControllersFull);
void Gsa2ControllersFullTo(JNIEnv *env, jobjectArray this, GSA2SDKControllersFull *sdkControllersFull);
void Gsa2ControllersFullRelease(JNIEnv *env, jobjectArray this, GSA2SDKControllersFull *sdkControllersFull);

#endif //LIBRARY_GSAUTH2_GSA2SCHEMA_H
