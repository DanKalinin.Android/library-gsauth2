//
// Created by Dan Kalinin on 9/5/20.
//

#ifndef LIBRARY_GSAUTH2_GSA2CLIENT_H
#define LIBRARY_GSAUTH2_GSA2CLIENT_H

#include "Gsa2Main.h"
#include "Gsa2Schema.h"
#include "Gsa2Database.h"
#include "Gsa2Exception.h"

typedef struct {
    jclass class;
    jfieldID object;
    jmethodID init;
} Gsa2ClientType;

extern Gsa2ClientType Gsa2Client;

void Gsa2ClientLoad(JNIEnv *env);
void Gsa2ClientUnload(JNIEnv *env);

// IdentityCheck

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientIdentityCheckCallbackType;

extern Gsa2ClientIdentityCheckCallbackType Gsa2ClientIdentityCheckCallback;

void Gsa2ClientIdentityCheckCallbackLoad(JNIEnv *env);
void Gsa2ClientIdentityCheckCallbackUnload(JNIEnv *env);

// CodeSend

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientCodeSendCallbackType;

extern Gsa2ClientCodeSendCallbackType Gsa2ClientCodeSendCallback;

void Gsa2ClientCodeSendCallbackLoad(JNIEnv *env);
void Gsa2ClientCodeSendCallbackUnload(JNIEnv *env);

// AccountCreate

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientAccountCreateCallbackType;

extern Gsa2ClientAccountCreateCallbackType Gsa2ClientAccountCreateCallback;

void Gsa2ClientAccountCreateCallbackLoad(JNIEnv *env);
void Gsa2ClientAccountCreateCallbackUnload(JNIEnv *env);

// AccountGet

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientAccountGetCallbackType;

extern Gsa2ClientAccountGetCallbackType Gsa2ClientAccountGetCallback;

void Gsa2ClientAccountGetCallbackLoad(JNIEnv *env);
void Gsa2ClientAccountGetCallbackUnload(JNIEnv *env);

// PasswordUpdate

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientPasswordUpdateCallbackType;

extern Gsa2ClientPasswordUpdateCallbackType Gsa2ClientPasswordUpdateCallback;

void Gsa2ClientPasswordUpdateCallbackLoad(JNIEnv *env);
void Gsa2ClientPasswordUpdateCallbackUnload(JNIEnv *env);

// DomainCreate

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientDomainCreateCallbackType;

extern Gsa2ClientDomainCreateCallbackType Gsa2ClientDomainCreateCallback;

void Gsa2ClientDomainCreateCallbackLoad(JNIEnv *env);
void Gsa2ClientDomainCreateCallbackUnload(JNIEnv *env);

// DomainRename

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientDomainRenameCallbackType;

extern Gsa2ClientDomainRenameCallbackType Gsa2ClientDomainRenameCallback;

void Gsa2ClientDomainRenameCallbackLoad(JNIEnv *env);
void Gsa2ClientDomainRenameCallbackUnload(JNIEnv *env);

// DomainDelete

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientDomainDeleteCallbackType;

extern Gsa2ClientDomainDeleteCallbackType Gsa2ClientDomainDeleteCallback;

void Gsa2ClientDomainDeleteCallbackLoad(JNIEnv *env);
void Gsa2ClientDomainDeleteCallbackUnload(JNIEnv *env);

// DomainExit

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientDomainExitCallbackType;

extern Gsa2ClientDomainExitCallbackType Gsa2ClientDomainExitCallback;

void Gsa2ClientDomainExitCallbackLoad(JNIEnv *env);
void Gsa2ClientDomainExitCallbackUnload(JNIEnv *env);

// GuestDelete

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientGuestDeleteCallbackType;

extern Gsa2ClientGuestDeleteCallbackType Gsa2ClientGuestDeleteCallback;

void Gsa2ClientGuestDeleteCallbackLoad(JNIEnv *env);
void Gsa2ClientGuestDeleteCallbackUnload(JNIEnv *env);

// InvitationCreate

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientInvitationCreateCallbackType;

extern Gsa2ClientInvitationCreateCallbackType Gsa2ClientInvitationCreateCallback;

void Gsa2ClientInvitationCreateCallbackLoad(JNIEnv *env);
void Gsa2ClientInvitationCreateCallbackUnload(JNIEnv *env);

// InvitationRevoke

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientInvitationRevokeCallbackType;

extern Gsa2ClientInvitationRevokeCallbackType Gsa2ClientInvitationRevokeCallback;

void Gsa2ClientInvitationRevokeCallbackLoad(JNIEnv *env);
void Gsa2ClientInvitationRevokeCallbackUnload(JNIEnv *env);

// InvitationAccept

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientInvitationAcceptCallbackType;

extern Gsa2ClientInvitationAcceptCallbackType Gsa2ClientInvitationAcceptCallback;

void Gsa2ClientInvitationAcceptCallbackLoad(JNIEnv *env);
void Gsa2ClientInvitationAcceptCallbackUnload(JNIEnv *env);

// CodeGet

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientCodeGetCallbackType;

extern Gsa2ClientCodeGetCallbackType Gsa2ClientCodeGetCallback;

void Gsa2ClientCodeGetCallbackLoad(JNIEnv *env);
void Gsa2ClientCodeGetCallbackUnload(JNIEnv *env);

// ControllersAdd

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientControllersAddCallbackType;

extern Gsa2ClientControllersAddCallbackType Gsa2ClientControllersAddCallback;

void Gsa2ClientControllersAddCallbackLoad(JNIEnv *env);
void Gsa2ClientControllersAddCallbackUnload(JNIEnv *env);

// ControllerRemove

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientControllerRemoveCallbackType;

extern Gsa2ClientControllerRemoveCallbackType Gsa2ClientControllerRemoveCallback;

void Gsa2ClientControllerRemoveCallbackLoad(JNIEnv *env);
void Gsa2ClientControllerRemoveCallbackUnload(JNIEnv *env);

// TokenIssue

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientTokenIssueCallbackType;

extern Gsa2ClientTokenIssueCallbackType Gsa2ClientTokenIssueCallback;

void Gsa2ClientTokenIssueCallbackLoad(JNIEnv *env);
void Gsa2ClientTokenIssueCallbackUnload(JNIEnv *env);

// TokenRevoke

typedef struct {
    jclass class;
    jmethodID callback;
} Gsa2ClientTokenRevokeCallbackType;

extern Gsa2ClientTokenRevokeCallbackType Gsa2ClientTokenRevokeCallback;

void Gsa2ClientTokenRevokeCallbackLoad(JNIEnv *env);
void Gsa2ClientTokenRevokeCallbackUnload(JNIEnv *env);

#endif //LIBRARY_GSAUTH2_GSA2CLIENT_H
